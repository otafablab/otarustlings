//! Your task is to fill in the generic parameters to `Person` and fix the
//! dangling reference issue however you wish.
//! 
//! Note that a `String` can never be static as it includes an allocation (the
//! crate lazy_static solves that issue, but is not needed here).

#[derive(Debug)]
struct Person<'a> {
    pub name: &'a str,
    pub age: i32,
}

fn make_person() -> Person<___> {
    let name = "Michael Stevens".to_string();

    Person {
        name: &name,
        age: 36,
    }
}

#[test]
fn main() {
    let michael = make_person();

    assert_eq!(michael.age, 36)
}
