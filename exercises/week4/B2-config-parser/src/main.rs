//! Your task is to add missing fields to `Config` which are present in
//! `config.toml` at `exercises/week4/BX-config-parser/config.toml`. You also
//! need to add new structs to accommodate the data.
//! 
//! You should start by reading what serde is about: https://serde.rs/.

#![allow(dead_code)] // In this example we don't use the parsed data

use std::{collections::HashMap, fs::read_to_string, net::Ipv4Addr, path::Path};

use serde::Deserialize;
use toml::{from_str, value::Datetime, Value};

#[derive(Debug, Deserialize)]
struct Config {
    title: String,
    servers: HashMap<String, Server>,
}

#[derive(Debug, Deserialize)]
struct Database {
    enabled: bool,
    ports: Vec<u16>,
    /// A vector of arbitrary toml values, can be heterogeneous
    data: Vec<Value>,
    temp_targets: ___,
}

struct Server {
    ip: Ipv4Addr,
    role: String,
}

impl Config {
    pub fn parse(path: impl AsRef<Path>) -> anyhow::Result<Self> {
        let path = path.as_ref();
        // TODO
    }
}

fn main() -> anyhow::Result<()> {
    let config: Config = from_str(&read_to_string("config.toml")?)?;

    println!("the parsed config is: {:#?}", config);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn name_is_tom() {
        let config = Config::parse("config.toml").unwrap();

        assert!(config.owner.name.starts_with("Tom"));
    }

    #[test]
    fn open_ports() {
        let config = Config::parse("config.toml").unwrap();

        assert_eq!(config.database.ports.len(), 3);
    }

    #[test]
    fn reasonable_temp_targets() {
        let config = Config::parse("config.toml").unwrap();

        assert!(config
            .database
            .temp_targets
            .iter()
            .all(|(_key, &target)| target < 100.0));
    }

    #[test]
    fn server_ips_different() {
        let config = Config::parse("config.toml").unwrap();

        let alpha = config.servers.get("alpha").unwrap();
        let beta = config.servers.get("beta").unwrap();

        assert_ne!(alpha.ip, beta.ip);
    }
}
