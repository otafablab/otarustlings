//! Your task is to derive required traits for `Foo`. You might find *the book*
//! helpful: https://doc.rust-lang.org/book/appendix-03-derivable-traits.html

// TODO
struct Foo {
    numbers: Vec<i32>,
}

#[test]
fn main() {
    let foo = Foo::default();
    dbg!(foo.clone());

    assert_eq!(foo, Foo { numbers: vec![] })
}
