//! Your task is to define the variants of the enumeration. Do not edit `main`.

#[derive(Debug)] // This makes printing the instances with {:?} (debug format) possible
enum Message {
    // TODO: define missing variants
}

#[test]
fn main() {
    println!("{:?}", Message::Quit);
    println!("{:?}", Message::Echo);
    println!("{:?}", Message::Move);
    println!("{:?}", Message::ChangeColor);
}
