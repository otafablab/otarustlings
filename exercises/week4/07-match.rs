//! Your task is to implement the match
//! 
//! Part 1: Write the match statement in `send_message` to "do the correct
//! stuff".
//! 
//! Part 2: You may need to add more messages to the array `messages`.

/// A position in 2D space
#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

/// State of the abstract high-level frobnicator
#[derive(Debug)]
struct State {
    location: Point,
    running: bool,
}

/// Messages sent to the [`State`] frobnicator
#[derive(Debug)]
enum Message {
    /// Stops the frobnicator
    Quit,
    /// Moves the frobnicator
    Move { x: i32, y: i32 },
}

impl State {
    fn new() -> Self {
        Self {
            location: Point { x: 0, y: 0 },
            running: true,
        }
    }

    fn send_message(&mut self, msg: Message) {
        // TODO
        match ___
    }
}

#[test]
fn main() {
    let messages = [
        Message::Move { x: 1, y: 3 },
        // TODO Send more messages
        ___
    ];

    let mut state = State::new();

    for msg in messages {
        state.send_message(msg);
    }

    assert_eq!(state.location, Point { x: -1, y: 2 });
    assert!(!state.running);
}
