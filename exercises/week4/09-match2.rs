//! Your task is to fill in the match in `execute`.

#[derive(Debug, PartialEq)]
enum Cli {
    Run(Option<String>),
    Rename(String, Option<String>),
}

impl Cli {
    pub fn execute(&self) -> Option<String> {
        use Cli::*; // Makes all variants visible without Cli prefix
        match self {
            Run(None) | ___ => None,
            // TODO
        }
    }
}

#[test]
fn main() {
    assert_eq!(
        Cli::Run(Some("ferris.rs".to_string())).execute().unwrap(),
        "running ferris.rs"
    );
    assert_eq!(
        Cli::Rename("ferris.rs".to_string(), Some("corro.c".to_string()))
            .execute()
            .unwrap(),
        "renaming ferris.rs to corro.c"
    );
    assert_eq!(Cli::Rename("/etc/shadow".to_string(), None).execute(), None);
    assert_eq!(Cli::Run(None).execute(), None);
}
