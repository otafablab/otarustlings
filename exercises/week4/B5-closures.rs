//! Your task is to fix the returned closure from `is_garbage`.

fn is_garbage() -> impl Fn(&str) -> bool {
    let no_garbage = vec!["C", "C++", "Rust"];
    |lang: &str| -> bool { !no_garbage.contains(&lang) }
}

#[test]
fn main() {
    let is_garbage = is_garbage();
    assert!(is_garbage("Java"));
    assert!(is_garbage("Python"));
    assert!(is_garbage("TypeScript"));
}
