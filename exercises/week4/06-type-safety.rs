//! Your task is convert `delete_filesystem` to use DryRun instead of a boolean.
//! Read more about type safety guidelines here
//! https://rust-lang.github.io/api-guidelines/type-safety.html

#[derive(Debug)]
enum DryRun {
    /// Does not commit any changes
    Yes,
    /// Danger: everything will be lost
    No,
}

fn delete_filesystem(dry_run: bool) -> bool {
    if dry_run == false {
        println!("Deleting all files...");
        true
    } else {
        println!("Not deleting files...");
        false
    }
}

#[test]
fn main() {
    let deleted = delete_filesystem(DryRun::No);

    assert_eq!(deleted, true);
}
