//! Your task is to implement `to_miles` for `Kilometers` and fill in the blank
//! in `main`.

// These structs are called newtype structs. Read more about the newtype design
// pattern:
// https://rust-unofficial.github.io/patterns/patterns/behavioural/newtype.html
struct Miles(f64);
struct Kilometers(f64);

impl Miles {
    fn to_kilometers(self) -> Kilometers {
        Kilometers(1.6 * self.0)
    }
}

impl Kilometers {
    // TODO
}

fn travel(distance: Miles) -> String {
    format!("Travelled {} miles", distance.0)
}

fn travel_kilometers(distance: Kilometers) -> String {
    travel(distance.to_miles())
}

#[test]
fn main() {
    let msg = travel_kilometers(___);

    assert_eq!(&msg, "Travelled 6.25 miles");
}
