//! Your task is to fill in the blanks. Don't change anything outside the `if
//! let`. Check https://doc.rust-lang.org/book/ch18-03-pattern-syntax.html
//! around "Destructuring Structs" for the answer.

enum Message {
    Quit,
    Move { x: i32, y: i32 },
}

#[test]
fn main() {
    let msg = Message::Move { x: 1, y: 2 };

    let mut x = 2;
    let mut y = 3;

    if let Message::Move { ___ } = msg {
        x += ___;
        _ += ___;
    }

    assert_eq!(x, 3);
    assert_eq!(y, 5);
}
