//! Your task is to fix the type errors and initialize the `numbers` array.
//!
//! Read more about Options in the book chapter 6.1
//! https://doc.rust-lang.org/book/ch06-01-defining-an-enum.html

// Do not change this function
fn print_number(maybe_number: Option<u16>) {
    println!("maybe {:?}", maybe_number);
}

#[test]
fn main() {
    print_number(13);
    print_number(99);

    let mut numbers: [Option<u16>; 5];

    for iter in 0..5 {
        let number_to_add: u16 = ((iter * 1235) + 2) / (4 * 16);

        numbers[iter as usize] = number_to_add;
    }

    assert_eq!(numbers, [Some(0), Some(19), Some(38), Some(57), Some(77)]);
}
