//! Your task is to implement `evaluate` for `Map`, and define the `square`
//! closure.

struct Map<F>
where
    F: Fn(i32) -> i32,
{
    data: Vec<i32>,
    mapper: F,
}

impl<F> Map<F>
where
    F: Fn(i32) -> i32,
{
    fn new(data: Vec<i32>, mapper: F) -> Self {
        Self { data, mapper }
    }

    /// Maps each element in `data` using `mapper` in-place
    fn evaluate(&mut self) {
        // TODO
    }

    fn into_vec(self) -> Vec<i32> {
        self.data
    }
}

#[test]
fn main() {
    let square = |_| ___;

    let mut map = Map::new(vec![1, 2, 3, 4], square);

    map.evaluate();
    map.evaluate();

    assert_eq!(map.into_vec(), [1, 16, 81, 256]);
}
