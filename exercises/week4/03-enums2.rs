//! Your task is to fix the `is_quit` function. Make good use of your favorite
//! feature of Rust: `match`. Bonus points if you can do it with `matches!`.

#[derive(Debug)]
enum Message {
    Quit,
    Echo,
    Move,
    ChangeColor,
}

// TODO Message::Quit is not a type indeed
fn is_quit(msg: Message::Quit) -> bool {
    true
}

#[test]
fn main() {
    assert!(is_quit(Message::Quit));
    assert!(!is_quit(Message::Echo));
}
