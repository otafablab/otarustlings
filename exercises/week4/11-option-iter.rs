//! Your task is to fill in all the blanks. Nothing else should need fixing.
//! Read more about iterators and their adaptors in
//! https://doc.rust-lang.org/book/ch13-02-iterators.html If too hard, try
//! harder.

/// Divides `dividend` by `divisor` safely, i.e. never divides by zero. A divide
/// by zero is represented with `None`
fn safe_div(dividend: i32, divisor: i32) -> Option<f32> {
    if divisor == 0 {
        None
    } else {
        Some(dividend as f32 / divisor as f32)
    }
}

/// Returns a list of `Some`s with all integers from -5 to 4 inside
fn list_of_options() -> Vec<i32> {
    (-5..5).into_iter().collect()
}

#[test]
fn main() {
    let list = list_of_options();

    let pair_fractions: Vec<Option<f32>> = list
        .windows(2)
        // TODO divide the previous number in the list by the next number
        // **safely**
        .map(___)
        .___;

    use std::convert::identity; // Super helpful morphism, right?

    // TODO `filter_map` is not implemented for a `Vec`, maybe convert to an
    // iterator first.
    let sum: ___ = pair_fractions.___.filter_map(___).___;

    assert_eq!(sum, 8.0);
}
