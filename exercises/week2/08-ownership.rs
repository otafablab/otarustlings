#[test]
fn main() {
    let vec0 = Vec::new();

    let vec1 = fill_vec(vec0);

    println!("vec1 has length {} content `{:?}`", vec1.len(), vec1);

    vec1.push(88);

    println!("ve1 has length {} content `{:?}`", vec1.len(), vec1);
}

fn fill_vec(vec: Vec<i32>) -> Vec<i32> {
    let mut vec = vec;

    vec.push(22);
    vec.push(44);
    vec.push(66);

    vec
}
