// Make me compile using shadowing: Add a `let` statement.
// No reordering of them lines is necessary.

#[test]
fn main() {
    let x = 100;
    let y = &x;
    x = x + 1000;
    println!("1000 + {} == {}", y, x);
}
