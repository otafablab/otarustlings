fn call_me(num: ___) {
    for i in 0..num {
        println!("Ring! Call number {}", i + 1);
    }
}

#[test]
fn main() {
    call_me(3);
}
