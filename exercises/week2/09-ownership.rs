// Make me compile only by reordering the lines in `main()`, but without
// adding, changing or removing any of them.

#[test]
fn main() {
    let mut x = 100;
    let y = &x;
    x += 1000;
    // Note: += is defined by the trait AddAssign:
    // https://doc.rust-lang.org/std/ops/trait.AddAssign.html
    println!("{} == {}", y, x);
}
