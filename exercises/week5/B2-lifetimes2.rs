//! Your task is to fix the lifetimes to be more generic and not restricted to
//! 'static only lifetimes. You need to add a new generic lifetime parameter to
//! the Reference trait definition.
//!
//! Note that the Reference trait is nothing special nor does it exist in the
//! standard library, I just made it up for this exercise.

trait Reference<T: ?Sized> {
    fn reference(self) -> &'static T;
}

fn foo(t: impl Reference<str>) -> &'static str {
    t.reference()
}

impl Reference<str> for &'static str {
    fn reference(self) -> &'static str {
        self
    }
}

// Do not edit anything below

#[test]
fn main() {
    let word = "bird".to_string(); // does not have a static lifetime
    let reference = &word;
    println!("{}", reference.reference());
}
