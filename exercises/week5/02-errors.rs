//! Your task is to fix the `new` method.

#[derive(PartialEq, Debug)]
struct PositiveInteger(u64);

#[derive(PartialEq, Debug)]
enum CreationError {
    Negative,
    Zero,
}

impl PositiveInteger {
    /// Creates a [`PositiveInteger`] from an `i64`.
    ///
    /// # Errors
    ///
    /// - [`CreationError::Negative`] is returned if the `value` is negative.
    /// - [`CreationError::Zero`] is returned if the `value` is zero.
    fn new(value: i64) -> Result<PositiveInteger, CreationError> {
        // FIXME
        Ok(PositiveInteger(value as u64))
    }
}

#[test]
fn test_creation() {
    assert!(PositiveInteger::new(10).is_ok());
    assert_eq!(Err(CreationError::Negative), PositiveInteger::new(-10));
    assert_eq!(Err(CreationError::Zero), PositiveInteger::new(0));
}
