//! Your task is to implement vector addition and scaling using built-in
//! operators `+` and `*`.
//!
//! See https://doc.rust-lang.org/std/ops/trait.Add.html and
//! https://doc.rust-lang.org/std/ops/trait.Mul.html for reference.

use std::ops::{Add, Mul};

#[derive(Debug, PartialEq)]
struct Vec3 {
    x: f32,
    y: f32,
    z: f32,
}

impl Vec3 {
    fn new(x: f32, y: f32, z: f32) -> Self {
        Self { x, y, z }
    }
}

// TODO

#[test]
fn add() {
    assert_eq!(
        Vec3::new(1.0, 2.0, -1.0) + Vec3::new(3.0, -2.0, 2.0),
        Vec3::new(4.0, 0.0, 1.0)
    );
}

#[test]
fn mul() {
    assert_eq!(2.0 * Vec3::new(3.0, -2.0, 2.0), Vec3::new(6.0, -4.0, 4.0));

    assert_eq!(Vec3::new(3.0, -2.0, 2.0) * 2.0, Vec3::new(6.0, -4.0, 4.0));
}
