//! Your task is to fix `create_fns` to return some kind of list of closures,
//! doesn't have to be an array.

fn create_fns() -> [Box<dyn Fn(&str)>; todo!()] {
    let print = |text| println!("{}", text);
    let print_uppercase = |text| println!("{}", text.to_uppercase());
    let sdrawkcab_tnirp = |txet| println!("{}", txet.chars().rev().collect::<String>());

    [todo!()]
}

// Don't edit anything below

#[test]
fn main() {
    let fns = create_fns();

    {
        for fun in &fns {
            let text = "template text";
            fun(&text);
        }
    }

    {
        for fun in &fns {
            let text = "other text";
            fun(&text);
        }
    }
}
