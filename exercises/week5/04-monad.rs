//! Your task is to rewrite `bad_multiply` without using `match` or `if left`.
//! Remember that `Option`s are monads, and monads are just monoids in the
//! category of endofunctors. Big deal ¯\_(ツ)¯\_

use std::num::ParseIntError;

fn bad_multiply(left: &str, right: &str) -> Result<i32, ParseIntError> {
    match left.parse::<i32>() {
        Ok(n1) => match right.parse::<i32>() {
            Ok(n2) => Ok(n1 * n2),
            Err(e) => Err(e),
        },
        Err(e) => Err(e),
    }
}

fn multiply(left: &str, right: &str) -> Result<i32, ParseIntError> {
    // TODO Rewrite `bad_multiply` using monadic features, like `and_then` and
    // `map` here.
    todo!()
}

#[test]
fn main() {
    // This still presents a reasonable answer.
    let twenty = multiply("10", "2");
    assert_eq!(twenty, Ok(20));

    // The following now provides a much more helpful error message.
    let not_twenty = multiply("ten", "2");
    assert!(matches!(not_twenty, Err(ParseIntError)));
}
