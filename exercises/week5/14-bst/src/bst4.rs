//! # Part - Final grind
//!
//! - [`BST::take`]
//! - [`BST::replace`]
//! - [`BST::take_max`]
//! - [`BST::remove`]

use crate::BST;
impl<T: Ord> BST<T> {
    /// Replaces `self` with [`Self::default`], returning the previous `self`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::<i32>::Empty;
    /// assert_eq!(bst.take(), BST::Empty);
    /// assert_eq!(bst, BST::Empty);
    ///
    /// let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// let right = match &mut bst {
    ///     BST::Node(_, _, right) => right.take(),
    ///     _ => unreachable!(),
    /// };
    /// assert_eq!(right, BST::leaf(3));
    /// assert_eq!(bst, BST::left(2, BST::leaf(1)));
    /// ```
    pub fn take(&mut self) -> Self {
        todo!()
    }

    /// Replaces `self` with `bst`, returning the previous `self`. Does (and
    /// can) not make sure BST laws are followed. Should be used with caution to
    /// avoid _bugs_.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::<i32>::Empty;
    /// assert_eq!(bst.replace(BST::leaf(1)), BST::Empty);
    /// assert_eq!(bst, BST::leaf(1));
    ///
    /// let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// let right = match &mut bst {
    ///     BST::Node(_, _, right) => right.replace(BST::leaf(10)),
    ///     _ => unreachable!(),
    /// };
    /// assert_eq!(right, BST::leaf(3));
    /// assert_eq!(bst, BST::node(2, BST::leaf(1), BST::leaf(10)));
    /// ```
    pub fn replace(&mut self, bst: Self) -> Self {
        todo!()
    }

    /// Returns the maximum value in the tree while removing it from `self`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::<()>::Empty;
    /// assert_eq!(bst.take_max(), None);
    /// assert_eq!(bst, BST::<()>::Empty);
    ///
    /// let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// assert_eq!(bst.take_max(), Some(3));
    /// assert_eq!(bst, BST::left(2, BST::leaf(1)));
    /// ```
    pub fn take_max(&mut self) -> Option<T> {
        todo!()
    }

    /// Removes and returns the owned `t` if it exists in the tree. Maintains
    /// the BST law. Scales with `O(log(n))`.
    ///
    /// Start by implement the procedure from
    /// https://en.wikipedia.org/wiki/Binary_search_tree#Deletion
    ///
    /// Challenge: The algorithm from wikipedia fails to take multiple minima
    /// into account. Find and remove the `#[ignore]` from the tests to start.
    ///
    /// # Examples
    ///
    /// ## One subtree
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::left(2, BST::left(2, BST::leaf(1)));
    /// assert!(bst.is_valid());
    /// //     2
    /// //    /
    /// //   2
    /// //  /
    /// // 1
    /// assert_eq!(bst.remove(&2), Some(2));
    ///
    /// assert_eq!(bst, BST::left(2, BST::leaf(1)));
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::right(1, BST::node(3, BST::leaf(2), BST::leaf(4)));
    /// assert!(bst.is_valid());
    /// // 1
    /// //  \
    /// //   3
    /// //  / \
    /// // 2   4
    /// assert_eq!(bst.remove(&1), Some(1));
    ///
    /// assert_eq!(bst, BST::node(3, BST::leaf(2), BST::leaf(4)));
    /// ```
    ///
    /// ## Two subtrees
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::node(4, BST::node(2, BST::leaf(1), BST::leaf(3)), BST::node(6, BST::leaf(5), BST::leaf(7)));
    /// assert!(bst.is_valid());
    /// //     .4.
    /// //    /   \
    /// //   2     6
    /// //  / \   / \
    /// // 1   3 5   7
    /// assert_eq!(bst.remove(&6), Some(6));
    /// assert!(bst.is_valid());
    /// assert_eq!(bst.len(), 6);
    ///
    /// assert_eq!(bst.remove(&4), Some(4));
    /// ```
    ///
    /// ## Challenge - takes multiple minima into account
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::new();
    /// [5, 4, 8, 3, 6, 3, 5].into_iter().for_each(|i| { bst.insert(i); });
    /// assert!(bst.is_valid());
    /// assert_eq!(bst.len(), 7);
    /// //       5
    /// //      / \
    /// //     4   8
    /// //    /   /
    /// //   3   6
    /// //  / \
    /// // 3   4
    /// assert_eq!(bst.remove(&5), Some(5));
    /// assert_eq!(bst.len(), 6);
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let mut bst = BST::new();
    /// [5, 0, 8, 8, 9, 6, 6, 7].into_iter().for_each(|i| { bst.insert(i); });
    /// assert!(bst.is_valid());
    /// assert_eq!(bst.len(), 8);
    /// //     5
    /// //    / \
    /// //   0   8
    /// //      / \
    /// //     8   9
    /// //    /
    /// //   6
    /// //  / \
    /// // 6   7
    /// assert_eq!(bst.remove(&5), Some(5));
    /// assert_eq!(bst.len(), 7);
    /// ```
    pub fn remove(&mut self, t: &T) -> Option<T> {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    // Re-export test helpers
    pub use super::super::tests::*;

    #[test]
    fn take() {
        let mut bst = BST::<i32>::Empty;
        assert_eq!(bst.take(), BST::Empty);
        assert_eq!(bst, BST::Empty);
        let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
        let right = match &mut bst {
            BST::Node(_, _, right) => right.take(),
            _ => unreachable!(),
        };
        assert_eq!(right, BST::leaf(3));
        assert_eq!(bst, BST::left(2, BST::leaf(1)));
    }

    #[test]
    fn replace() {
        let mut bst = BST::<i32>::Empty;
        assert_eq!(bst.replace(BST::leaf(1)), BST::Empty);
        assert_eq!(bst, BST::leaf(1));
        let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
        let right = match &mut bst {
            BST::Node(_, _, right) => right.replace(BST::leaf(10)),
            _ => unreachable!(),
        };
        assert_eq!(right, BST::leaf(3));
        assert_eq!(bst, BST::node(2, BST::leaf(1), BST::leaf(10)));
    }

    #[test]
    fn take_max() {
        let mut bst = BST::<()>::Empty;
        assert_eq!(bst.take_max(), None);
        assert_eq!(bst, BST::<()>::Empty);
        let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
        assert_eq!(bst.take_max(), Some(3));
        assert_eq!(bst, BST::left(2, BST::leaf(1)));
    }

    mod remove {
        use super::*;
        #[test]
        fn remove_empty() {
            let mut bst = BST::default();
            assert_eq!(bst.remove(&10), None);
            assert_eq!(bst, BST::Empty);
        }
        #[test]
        fn remove_leaf() {
            let mut bst = BST::right(1, BST::leaf(2));
            assert_eq!(bst.remove(&2), Some(2));
            assert_eq!(bst, BST::leaf(1));

            let mut bst = BST::left(2, BST::left(2, BST::leaf(1)));
            assert_eq!(bst.remove(&2), Some(2));
            assert_eq!(bst, BST::left(2, BST::leaf(1)));
        }
        #[test]
        fn remove_one_subtree() {
            let mut bst = BST::right(1, BST::node(3, BST::leaf(2), BST::leaf(4)));
            assert_eq!(bst.remove(&1), Some(1));
            // 1
            //  \
            //   3
            //  / \
            // 2   4
            assert_eq!(bst, BST::node(3, BST::leaf(2), BST::leaf(4)));
        }
        #[test]
        fn remove_two_subtrees() {
            let mut bst = bst4();
            let len = bst.len();
            assert_eq!(bst.remove(&5), Some(5));
            // Not doing further testing to allow many implementations to pass
            assert_eq!(bst.len(), len - 1);

            println!(
                "You have completed bst! If you want to take on the challenge, remove the #[ignore] from the last test."
            );
        }
        #[test]
        // Remove the following line to see if your solution passes the
        // challenge
        #[ignore = "challenge"]
        fn challenge() {
            let mut bst = BST::new();
            [5, 4, 8, 3, 6, 3, 5].into_iter().for_each(|i| {
                bst.insert(i);
            });
            assert!(bst.is_valid());
            assert_eq!(bst.len(), 7);
            //       5
            //      / \
            //     4   8
            //    /   /
            //   3   6
            //  / \
            // 3   4
            assert_eq!(bst.remove(&5), Some(5));
            assert_eq!(bst.len(), 6);

            let mut bst = BST::new();
            [5, 0, 8, 8, 9, 6, 6, 7].into_iter().for_each(|i| {
                bst.insert(i);
            });
            assert!(bst.is_valid());
            assert_eq!(bst.len(), 8);
            //     5
            //    / \
            //   0   8
            //      / \
            //     8   9
            //    /
            //   6
            //  / \
            // 6   7
            assert_eq!(bst.remove(&5), Some(5));
            assert_eq!(bst.len(), 7);
        }
    }
}
