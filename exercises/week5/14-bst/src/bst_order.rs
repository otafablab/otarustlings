use crate::BST;

/// Enum describing in which order are values of the tree collected into a
/// one-dimensional list.
#[derive(Debug, Clone, Copy)]
pub enum BstOrder {
    /// Push node value pre-iteration.
    Pre,
    /// Push node value after iterating the left tree is finished. Visually
    /// corresponds to pushing when going under the node.
    In,
    /// Push node value after iterating both sub-trees.
    Post,
    /// Same as [`BstOrder::In`] but in reverse.
    Reverse,
}

impl BstOrder {
    /// Fill `vec` with references to all values in `bst` in the [`BstOrder`].
    pub fn fill<'bst, T: Ord>(&self, vec: &mut Vec<&'bst T>, bst: &'bst BST<T>) {
        use BstOrder::*;
        match bst {
            BST::Empty => {}
            BST::Node(val, left, right) => match self {
                Pre => {
                    vec.push(val);
                    self.fill(vec, left);
                    self.fill(vec, right);
                }
                In => {
                    self.fill(vec, left);
                    vec.push(val);
                    self.fill(vec, right);
                }
                Post => {
                    self.fill(vec, left);
                    self.fill(vec, right);
                    vec.push(val);
                }
                Reverse => {
                    self.fill(vec, right);
                    vec.push(val);
                    self.fill(vec, left);
                }
            },
        }
    }

    /// Fill `vec` with all values in `bst` in the [`BstOrder`].
    pub fn fill_into<T: Ord>(&self, vec: &mut Vec<T>, bst: BST<T>) {
        use BstOrder::*;
        match bst {
            BST::Empty => {}
            BST::Node(val, left, right) => match self {
                Pre => {
                    vec.push(val);
                    self.fill_into(vec, *left);
                    self.fill_into(vec, *right);
                }
                In => {
                    self.fill_into(vec, *left);
                    vec.push(val);
                    self.fill_into(vec, *right);
                }
                Post => {
                    self.fill_into(vec, *left);
                    self.fill_into(vec, *right);
                    vec.push(val);
                }
                Reverse => {
                    self.fill_into(vec, *right);
                    vec.push(val);
                    self.fill_into(vec, *left);
                }
            },
        }
    }
}
