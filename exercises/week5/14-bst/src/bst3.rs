//! # Part 3 - Metrics and insert
//!
//! - [`BST::total_cost`]
//! - [`BST::average_cost`]
//! - [`BST::is_valid`]
//! - [`BST::is_degenerate`]
//! - [`BST::insert`]
//! - [`BST::contains`]
//! - [`BST::count`]
//! - [`BST::min`]
//! - [`BST::max`]
//! - [`BST::prepend`]
//! - [`BST::append`]

use std::cmp::Ordering;

// Uncomment the following when you are ready and the tests are passing.
// #[path = "bst4.rs"]
// mod bst4;

use crate::BST;
impl<T: Ord> BST<T> {
    /// Counts the total number of read/access operations required to reach each
    /// node **independently**. Each node at a depth `N` takes `N` operations to
    /// reach.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::right(1, BST::right(2, BST::leaf(3)));
    /// // 1         takes 1 step
    /// //  \
    /// //   2       takes 2 steps
    /// //    \
    /// //     3     takes 3 steps
    /// // average cost := 1 + 2 + 3 = 6
    /// assert_eq!(bst.total_cost(), 6);
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// //   2       takes 1 step
    /// //  / \
    /// // 1   3     takes 2 steps each
    /// // average cost := 1 + 2 * 2 = 5
    /// assert_eq!(bst.total_cost(), 5);
    /// ```
    pub fn total_cost(&self) -> usize {
        todo!()
    }

    /// Returns the mean number of read/access operations required to reach each
    /// node **indepently** (the total cost divided by the number of nodes). The
    /// average cost is smaller for more balanced trees and larger for
    /// degenerate trees. An empty tree does not have average cost.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::right(1, BST::right(2, BST::leaf(3)));
    /// // 1         takes 1 step
    /// //  \
    /// //   2       takes 2 steps
    /// //    \
    /// //     3     takes 3 steps
    /// // average cost := (1 + 2 + 3) / 3 = 2.0
    /// assert_eq!(bst.average_cost(), Some(2.0));
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// //   2       takes 1 step
    /// //  / \
    /// // 1   3     takes 2 steps each
    /// // average cost := (1 + 2 * 2) / 3 = 1.66
    /// assert_eq!(bst.average_cost(), Some(5.0 / 3.0));
    /// ```
    pub fn average_cost(&self) -> Option<f32> {
        todo!()
    }

    /// Returns whether this BST satisfies the BST law:
    /// - All values in the left subtrees must be smaller than or equal to their
    ///   parent
    /// - All values in the right subtrees must be greater than their parent
    ///
    /// An empty tree is valid.
    ///
    /// Note: avoid using [`Self::all`] as it causes unnecessary comparisons.
    pub fn is_valid(&self) -> bool {
        todo!()
    }

    /// Returns whether the tree is degenerate, i.e. it's just a linked list.
    /// Degenerate trees should be avoided in general as they are not _height
    /// balanced_ and have worse time complexity: `O(n)` instead of `O(log(n))`.
    ///
    /// An empty tree is not degenerate, but a single node is.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::right(1, BST::right(2, BST::leaf(3)));
    /// // 1
    /// //  \
    /// //   2
    /// //    \
    /// //     3
    /// assert!(bst.is_degenerate());
    ///
    /// let bst = BST::left(2, BST::leaf(1));
    /// //   2
    /// //  /
    /// // 1
    /// assert!(bst.is_degenerate());
    ///
    /// let bst = BST::left(3, BST::right(1, BST::leaf(2)));
    /// //   3
    /// //  /
    /// // 1
    /// //  \
    /// //   2
    /// assert!(bst.is_degenerate());
    ///
    /// let bst = BST::left(3, BST::node(2, BST::leaf(1), BST::leaf(3)));
    /// //     3
    /// //    /
    /// //   2
    /// //  / \
    /// // 1   3
    /// assert!(bst.is_degenerate().not());
    /// ```
    pub fn is_degenerate(&self) -> bool {
        todo!()
    }

    /// Inserts the `t` maintaining the BST law. Returns whether the inserted
    /// value is a duplicate. Scales with `O(log(n))`.
    ///
    /// Note: Keep the implementation simple: the value doesn't need to be
    /// "smartly" inserted at the shortest path. Insert shouldn't change any
    /// other parts of the tree.
    ///
    /// # Examples
    ///
    /// ## Inserting creates a new leaf
    ///
    /// ```text
    ///                         2
    ///    2                   /
    ///   /   insert(1) =>    2
    ///  2                   /
    ///                     1
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let mut bst = BST::left(2, BST::leaf(2));
    /// assert!(bst.insert(1).not()); // is not duplicate
    ///
    /// assert_eq!(bst, BST::left(2, BST::left(2, BST::leaf(1))));
    /// ```
    ///
    /// ## Inserting duplicates inserts to the left
    ///
    /// ```text
    ///  1                      1                   
    ///   \                    / \                  
    ///    3    insert(1) =>  1   3                             
    ///   / \                    / \                
    ///  2   4                  2   4   
    /// ```
    ///
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;                
    /// let mut bst = BST::right(1, BST::node(3, BST::leaf(2), BST::leaf(4)));
    /// assert!(bst.is_valid());
    /// assert!(bst.insert(1)); // is duplicate
    ///
    /// assert_eq!(bst, BST::node(1, BST::leaf(1), BST::node(3, BST::leaf(2), BST::leaf(4))));
    /// ```
    ///
    /// ## Inserting duplicates might not end up next to each other
    ///
    /// ```text
    ///                         1                   
    ///  1                       \                  
    ///   \     insert(3) =>      3                             
    ///    3                     / \                
    ///   / \                   2   4   
    ///  2   4                   \
    ///                           3
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;                
    /// let mut bst = BST::right(1, BST::node(3, BST::leaf(2), BST::leaf(4)));
    /// assert!(bst.is_valid());
    /// assert!(bst.insert(3)); // is duplicate
    ///
    /// assert_eq!(bst, BST::right(1, BST::node(3, BST::right(2, BST::leaf(3)), BST::leaf(4))));
    /// ```
    pub fn insert(&mut self, t: T) -> bool {
        todo!()
    }

    /// Returns whether `t` is in the tree or not. Scales with `O(log(n))`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::left(1, BST::right(0, BST::leaf(1)));
    /// assert!(bst.contains(&1));
    /// assert!(bst.contains(&0));
    /// assert!(bst.contains(&2).not());
    /// ```
    pub fn contains(&self, t: &T) -> bool {
        todo!()
    }

    /// Counts the number of `t`s in the tree.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::left(1, BST::right(0, BST::leaf(1)));
    /// assert_eq!(bst.count(&1), 2);
    /// assert_eq!(bst.count(&2), 0);
    /// ```
    pub fn count(&self, t: &T) -> usize {
        todo!()
    }

    /// Returns the minimum value or `None` if empty. Scales with `O(log(n))`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::<i32>::Empty;
    /// assert_eq!(bst.min(), None);
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::left(1, BST::right(0, BST::leaf(1)));
    /// assert_eq!(bst.min(), Some(&0));
    /// ```
    pub fn min(&self) -> Option<&T> {
        todo!()
    }

    /// Returns the maximum value or `None` if empty. Scales with `O(log(n))`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::<i32>::Empty;
    /// assert_eq!(bst.max(), None);
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let bst = BST::left(2, BST::right(0, BST::leaf(1)));
    /// assert_eq!(bst.max(), Some(&2));
    /// ```
    pub fn max(&self) -> Option<&T> {
        todo!()
    }

    /// Prepends (pushes left) the node with the minimum value with `bst`. Does
    /// not enforce BST laws.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let mut bst = BST::<i32>::Empty;
    /// bst.prepend(BST::leaf(1));
    /// assert_eq!(bst, BST::leaf(1));
    /// assert!(bst.is_valid());
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// bst.prepend(BST::leaf(100)); // breaks BST law
    /// assert_eq!(bst, BST::node(2, BST::left(1, BST::leaf(100)), BST::leaf(3)));
    /// assert!(bst.is_valid().not());
    /// ```
    pub fn prepend(&mut self, bst: Self) {
        todo!()
    }

    /// Appends (pushes right) the node with the maximum value with `bst`. Does
    /// not enforce BST laws.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let mut bst = BST::<i32>::Empty;
    /// bst.append(BST::leaf(1));
    /// assert_eq!(bst, BST::leaf(1));
    /// assert!(bst.is_valid());
    /// ```
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// let mut bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// bst.append(BST::leaf(0)); // breaks BST law
    /// assert_eq!(bst, BST::node(2, BST::leaf(1), BST::right(3, BST::leaf(0))));
    /// assert!(bst.is_valid().not());
    /// ```
    pub fn append(&mut self, bst: Self) {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use std::{ops::Not, sync::mpsc::channel, thread::spawn};

    use crate::utils::{CountComparisons, Message};

    use super::*;
    // Re-export test helpers
    pub use super::super::tests::*;
    #[test]
    fn total_cost() {
        assert_eq!(BST::<()>::Empty.total_cost(), 0);
        assert_eq!(bst1().total_cost(), 6);
        assert_eq!(bst2().total_cost(), 6);
        assert_eq!(bst3().total_cost(), 15);
        assert_eq!(bst4().total_cost(), 17);
        assert_eq!(bst5().total_cost(), 10);
    }

    #[test]
    fn average_cost() {
        assert_eq!(BST::<()>::Empty.average_cost(), None);
        assert_eq!(bst1().average_cost(), Some(2.0));
        assert_eq!(bst2().average_cost(), Some(2.0));
        assert_eq!(bst3().average_cost(), Some(3.0));
        assert_eq!(bst4().average_cost(), Some(17.0 / 7.0));
        assert_eq!(bst5().average_cost(), Some(2.5));
    }

    #[test]
    fn is_valid() {
        assert!(BST::<()>::Empty.is_valid());

        let bst = BST::node(
            4,
            BST::node(2, BST::leaf(1), BST::leaf(3)),
            BST::node(6, BST::leaf(5), BST::leaf(7)),
        );
        //     .4.
        //    /   \
        //   2     6
        //  / \   / \
        // 1   3 5   7
        assert!(bst.is_valid());

        let bst = BST::node(
            4,
            BST::node(2, BST::leaf(1), BST::leaf(5 /* should be smaller than 4 */)),
            BST::node(6, BST::leaf(5), BST::leaf(7)),
        );
        //     .4.
        //    /   \
        //   2     6
        //  / \   / \
        // 1   5 5   7
        assert!(bst.is_valid().not());

        let bst = BST::node(
            4,
            BST::node(
                2,
                BST::leaf(3 /* should be smaller than or equal to 2 */),
                BST::leaf(3),
            ),
            BST::node(6, BST::leaf(5), BST::leaf(7)),
        );
        //     .4.
        //    /   \
        //   2     6
        //  / \   / \
        // 3   3 5   7
        assert!(bst.is_valid().not());

        let bst = BST::node(
            4,
            BST::node(2, BST::leaf(1), BST::leaf(3)),
            BST::node(6, BST::leaf(4 /* should be greater than 4 */), BST::leaf(7)),
        );
        //     .4.
        //    /   \
        //   2     6
        //  / \   / \
        // 1   3 4   7
        assert!(bst.is_valid().not());
    }

    #[test]
    fn is_valid_comparisons() {
        let (tx, rx) = channel();

        let jh = spawn(move || {
            let mut counter = 0;
            while let Ok(message) = rx.recv() {
                match message {
                    Message::Increment => counter += 1,
                    Message::Clear => counter = 0,
                }
            }
            counter
        });

        spawn(move || {
            let mut bst = BST::new();
            let exp = 12;
            (0..exp)
                .flat_map(|n| (1..2u32.pow(n)).map(move |m| m * 2u32.pow(exp) / 2u32.pow(n)))
                .for_each(|i| {
                    bst.insert(CountComparisons::with_sender(i, tx.clone()));
                });

            tx.send(Message::Clear).unwrap(); // only count comparisons in is_valid
            assert!(bst.is_valid());
        })
        .join()
        .unwrap();

        let comparisons = jh.join().unwrap();
        assert!(
            comparisons <= 4082,
            "{} comparisons was greater than 4082",
            comparisons
        );
    }

    #[test]
    fn is_degenerate() {
        assert!(BST::<()>::Empty.is_degenerate().not());

        assert!(bst1().is_degenerate());
        assert!(bst2().is_degenerate());
        assert!(bst3().is_degenerate());
        assert!(bst4().is_degenerate().not());
        assert!(bst5().is_degenerate());
    }

    mod insert {
        use super::*;
        #[test]
        fn insert_empty() {
            let mut bst = BST::default();
            assert!(bst.insert(0).not());
            assert_eq!(bst, BST::leaf(0));
        }
        #[test]
        fn insert_bst1() {
            let mut bst = bst1();

            assert!(bst.insert(4).not());
            assert!(bst.is_valid());

            assert!(bst.insert(4));
            assert!(bst.is_valid());

            assert!(bst.insert(0).not());
            assert!(bst.is_valid());

            assert!(bst.insert(0));
            assert!(bst.is_valid());
        }
        #[test]
        fn insert_bst2() {
            let mut bst = bst2();

            assert!(bst.insert(4).not());
            assert!(bst.is_valid());

            assert!(bst.insert(4));
            assert!(bst.is_valid());

            assert!(bst.insert(0).not());
            assert!(bst.is_valid());

            assert!(bst.insert(0));
            assert!(bst.is_valid());
        }
        #[test]
        fn insert_bst3() {
            let mut bst = bst3();

            assert!(bst.insert(3));
            assert!(bst.is_valid());

            // 1
            //  \
            //   5
            //  /
            // 2
            //  \
            //   3
            //  / \
            // 3   4
            let expected = BST::right(
                1,
                BST::left(5, BST::right(2, BST::node(3, BST::leaf(3), BST::leaf(4)))),
            );
            assert_eq!(bst, expected);
        }
        #[test]
        fn insert_bst4() {
            let mut bst = bst4();

            assert!(bst.insert(3));
            assert!(bst.is_valid());
            assert!(bst.insert(5));
            assert!(bst.is_valid());
            assert!(bst.insert(8));
            assert!(bst.is_valid());

            //       .-5-.
            //      /     \
            //     3       7
            //    / \     / \
            //   2   4   6   8
            //    \   \     /
            //     3   5   8
            let expected = BST::node(
                5,
                BST::node(3, BST::right(2, BST::leaf(3)), BST::right(4, BST::leaf(5))),
                BST::node(7, BST::leaf(6), BST::left(8, BST::leaf(8))),
            );
            assert_eq!(bst, expected);
        }

        #[test]
        fn insert_comparisons() {
            let (tx, rx) = channel();

            let jh = spawn(move || {
                let mut counter = 0;
                while let Ok(message) = rx.recv() {
                    match message {
                        Message::Increment => counter += 1,
                        Message::Clear => counter = 0,
                    }
                }
                counter
            });

            spawn(move || {
                let mut bst = BST::new();
                let exp = 12;
                (0..exp)
                    .flat_map(|n| (1..2u32.pow(n)).map(move |m| m * 2u32.pow(exp) / 2u32.pow(n)))
                    .for_each(|i| {
                        bst.insert(CountComparisons::with_sender(i, tx.clone()));
                    });
            })
            .join()
            .unwrap();

            let comparisons = jh.join().unwrap();
            assert!(
                comparisons <= 53211,
                "{} comparisons was greater than 53211",
                comparisons
            );
        }
    }

    #[test]
    fn contains() {
        assert!(BST::<()>::Empty.contains(&()).not());

        assert!(bst1().contains(&3));
        assert!(bst1().contains(&0).not());
        assert!(bst2().contains(&3));
        assert!(bst3().contains(&4));
        assert!(bst4().contains(&6));
        assert!(bst5().contains(&5));
    }

    #[test]
    fn contains_comparisons() {
        let (tx, rx) = channel();

        let jh = spawn(move || {
            let mut counter = 0;
            while let Ok(message) = rx.recv() {
                match message {
                    Message::Increment => counter += 1,
                    Message::Clear => counter = 0,
                }
            }
            counter
        });

        spawn(move || {
            let mut bst = BST::new();
            let exp = 12;
            (0..exp)
                .flat_map(|n| (1..2u32.pow(n)).map(move |m| m * 2u32.pow(exp) / 2u32.pow(n)))
                .for_each(|i| {
                    bst.insert(CountComparisons::with_sender(i, tx.clone()));
                });
            tx.send(Message::Clear).unwrap(); // only count comparisons in contains
            assert!(bst
                .contains(&CountComparisons::with_sender(2u32.pow(exp) - 1, tx))
                .not());
        })
        .join()
        .unwrap();

        let comparisons = jh.join().unwrap();
        assert!(
            comparisons <= 11,
            "{} comparisons was greater than 11",
            comparisons
        );
    }

    #[test]
    fn count() {
        assert_eq!(BST::<()>::Empty.count(&()), 0);

        assert_eq!(bst1().count(&3), 1);
        assert_eq!(bst1().count(&0), 0);
        assert_eq!(bst2().count(&3), 1);
        assert_eq!(bst3().count(&4), 1);
        assert_eq!(bst4().count(&6), 1);
        assert_eq!(bst5().count(&5), 4);

        let mut bst = bst4();
        assert_eq!(bst.count(&100), 0);
        assert_eq!(bst.count(&3), 1);
        assert_eq!(bst.count(&5), 1);
        assert_eq!(bst.count(&8), 1);

        bst.insert(3);
        bst.insert(8);
        bst.insert(5);

        assert_eq!(bst.count(&3), 2);
        assert_eq!(bst.count(&5), 2);
        assert_eq!(bst.count(&8), 2);
    }

    #[test]
    fn count_comparisons() {
        let (tx, rx) = channel();

        let jh = spawn(move || {
            let mut counter = 0;
            while let Ok(message) = rx.recv() {
                match message {
                    Message::Increment => counter += 1,
                    Message::Clear => counter = 0,
                }
            }
            counter
        });

        spawn(move || {
            let mut bst = BST::new();
            let exp = 14;
            (0..exp)
                .flat_map(|n| (1..2u32.pow(n)).map(move |m| m * 2u32.pow(12) / 2u32.pow(n)))
                .for_each(|i| {
                    bst.insert(CountComparisons::with_sender(i, tx.clone()));
                });
            tx.send(Message::Clear).unwrap(); // only count comparisons in contains
            assert_eq!(bst.count(&CountComparisons::with_sender(1337, tx)), 3);
        })
        .join()
        .unwrap();

        let comparisons = jh.join().unwrap();
        assert!(
            comparisons <= 19,
            "{} comparisons was greater than 19",
            comparisons
        );
    }

    #[test]
    fn min() {
        assert_eq!(BST::<()>::Empty.min(), None);

        assert_eq!(bst1().min(), Some(&1));
        assert_eq!(bst2().min(), Some(&1));
        assert_eq!(bst3().min(), Some(&1));
        assert_eq!(bst4().min(), Some(&2));
        assert_eq!(bst5().min(), Some(&5));
    }

    #[test]
    fn max() {
        assert_eq!(BST::<()>::Empty.max(), None);

        assert_eq!(bst1().max(), Some(&3));
        assert_eq!(bst2().max(), Some(&3));
        assert_eq!(bst3().max(), Some(&5));
        assert_eq!(bst4().max(), Some(&8));
        assert_eq!(bst5().max(), Some(&5));
    }

    #[test]
    fn prepend() {
        let mut bst = BST::<()>::Empty;
        bst.prepend(BST::<()>::Empty);
        assert_eq!(bst, BST::<()>::Empty);

        let mut bst = BST::<i32>::Empty;
        bst.prepend(bst4());
        assert_eq!(bst, bst4());

        let mut bst = bst4();
        bst.prepend(BST::Empty);
        assert_eq!(bst, bst4());

        let mut bst = bst5();
        bst.prepend(bst2());
        assert_eq!(
            bst,
            BST::left(5, BST::left(5, BST::left(5, BST::left(5, bst2()))))
        );
        assert!(bst.is_valid());

        let mut bst = bst3();
        bst.prepend(bst4()); // this will not result in a valid BST

        assert_eq!(
            bst,
            BST::node(
                1,
                bst4(),
                BST::left(5, BST::right(2, BST::right(3, BST::leaf(4))))
            )
        );
        assert!(bst.is_valid().not());
    }

    #[test]
    fn append() {
        let mut bst = BST::<()>::Empty;
        bst.append(BST::<()>::Empty);
        assert_eq!(bst, BST::<()>::Empty);

        let mut bst = BST::<i32>::Empty;
        bst.append(bst4());
        assert_eq!(bst, bst4());

        let mut bst = bst4();
        bst.append(BST::Empty);
        assert_eq!(bst, bst4());

        let mut bst = bst2();
        bst.append(bst5());
        assert_eq!(bst, BST::right(1, BST::right(2, BST::right(3, bst5()))));
        assert!(bst.is_valid());

        let mut bst = bst3();
        bst.append(bst4()); // this will not result in a valid BST

        assert_eq!(
            bst,
            BST::right(
                1,
                BST::node(5, BST::right(2, BST::right(3, BST::leaf(4))), bst4())
            )
        );
        assert!(bst.is_valid().not());

        println!(
            "You have completed Part 3 of bst. Uncomment `// mod` from the start of this file."
        );
    }
}
