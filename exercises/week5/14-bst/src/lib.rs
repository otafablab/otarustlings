//! Your task is to implement a binary search tree data structure.
//!
//! # Overview
//!
//! A binary tree can be empty, or consist of a root value, a left binary tree
//! and a right binary tree. Binary trees can contain 0, 1 or 2 subtrees. You
//! will be implementing a binary _search_ tree which is a binary tree that
//! follows an ordering rule.
//!
//! The BST rules:
//!
//! - The value in the left subtree must be smaller than or equal to its parent
//! - The value in the right subtree must be larger than its parent
//! - Both subtrees follow the same BST rules
//!
//! Read this overview on Wikipedia:
//! <https://en.wikipedia.org/wiki/Binary_search_tree#Overview>
//!
//! Then read this article about binary search trees:
//! <https://austingwalters.com/binary-trees-traversals-everyday-algorithms/>
//!
//! This exercise is very extensive and will take many hours to complete, thus
//! it is split into four parts as modules. Start with `bst1.rs`.
//!
//! Once you are ready and the tests are passing, uncomment the `// mod` at the
//! start of the file.
//!
//! # Help
//!
//! Even though using recursion is not ideal in Rust, you should use it in this
//! exercise.
//! 
//! Some reference material for guidance:
//! - <https://en.wikipedia.org/wiki/Binary_search_tree>
//! - <https://docs.rs/binary_search_tree/0.2.2/binary_search_tree/struct.BinarySearchTree.html>
//!   (do not use this library but feel free to read its source code)
//!
//! Extra reading about balanced binary search trees:
//! - <https://en.wikipedia.org/wiki/AVL_tree>

pub mod bst1;
pub use bst1::BST;

pub mod utils;
