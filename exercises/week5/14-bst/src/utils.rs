use std::{cmp::Ordering, sync::mpsc::Sender};

#[derive(Clone, Debug)]
pub struct CountComparisons<T: Ord> {
    inner: T,
    sender: Sender<Message>,
}

#[derive(Clone, Copy, Debug)]
pub enum Message {
    Increment,
    Clear,
}

impl<T: Ord> PartialEq for CountComparisons<T> {
    fn eq(&self, other: &Self) -> bool {
        // == does not count as a comparison for us
        self.inner == other.inner
    }
}

impl<T: Ord> Eq for CountComparisons<T> {}

impl<T: Ord> PartialOrd for CountComparisons<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.sender.send(Message::Increment).unwrap();
        self.inner.partial_cmp(&other.inner)
    }
}

impl<T: Ord> Ord for CountComparisons<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.sender.send(Message::Increment).unwrap();
        self.inner.cmp(&other.inner)
    }
}

impl<T: Ord> CountComparisons<T> {
    pub fn with_sender(inner: T, sender: Sender<Message>) -> Self {
        Self { inner, sender }
    }
}
