//! NOTE: Read lib.rs first.
//!
//! # Part 1 - Constructors
//!
//! - [`BST::new`]
//! - [`BST::node`]
//! - [`BST::leaf`]
//! - [`BST::left`]
//! - [`BST::right`]

// Uncomment the following when you are ready and the tests are passing.
// #[path = "bst2.rs"]
// mod bst2;

/// A binary search tree (BST). The contained type must have a total order
/// [`Ord`].
///
/// # Examples of BSTs
///
/// The following is a valid binary search tree of height 4:
///
/// ```text
///      .-3-.
///     /     \
///    2       5
///   / \     / \
///  1   3   5   6
///         /
///        4
/// ```
///
/// The following is not a valid binary search tree as its right subtree must
/// contain only greater values than the root:
///
/// ```text
///    3
///   / \
///  2   4
///     /
///    3
/// ```
#[derive(Debug, Clone, PartialEq)]
pub enum BST<T: Ord> {
    Empty,
    Node(T, Box<Self>, Box<Self>),
}

impl<T: Ord> Default for BST<T> {
    fn default() -> Self {
        Self::Empty
    }
}

impl<T: Ord> BST<T> {
    /// Creates a new empty BST.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::<i32>::new();
    /// assert_eq!(bst, BST::Empty);
    /// ```
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates a leaf of the tree with only value `t` and no subtrees.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::leaf(42);
    /// assert_eq!(bst, BST::Node(42, Box::new(BST::Empty), Box::new(BST::Empty)));
    /// ```
    pub fn leaf(t: T) -> Self {
        todo!()
    }

    /// Creates a tree with value `t`, a `left` and a `right` subtree.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::node(2, BST::leaf(1), BST::leaf(3));
    /// assert_eq!(bst, BST::Node(2, Box::new(BST::leaf(1)), Box::new(BST::leaf(3))));
    /// ```
    pub fn node(t: T, left: Self, right: Self) -> Self {
        todo!()
    }

    /// Creates a node with value `t` and a `left` subtree. Right subtree is
    /// empty.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::left(2, BST::node(1, BST::leaf(0), BST::leaf(2)));
    /// assert_eq!(bst, BST::node(2, BST::node(1, BST::leaf(0), BST::leaf(2)), BST::Empty));
    /// ```
    pub fn left(t: T, left: Self) -> Self {
        todo!()
    }

    /// Creates a node with value `t` and a `right` subtree. Left subtree is
    /// empty.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// let bst = BST::right(2, BST::node(1, BST::leaf(0), BST::leaf(2)));
    /// assert_eq!(bst, BST::node(2, BST::Empty, BST::node(1, BST::leaf(0), BST::leaf(2))));
    /// ```
    pub fn right(t: T, right: Self) -> Self {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    /// Creates a tree of only lefts
    /// ```text
    ///     3
    ///    /
    ///   2
    ///  /
    /// 1
    /// ```
    pub fn bst1() -> BST<i32> {
        BST::left(3, BST::left(2, BST::leaf(1)))
    }

    /// Creates a tree of only rights
    /// ```text
    /// 1
    ///  \
    ///   2
    ///    \
    ///     3
    /// ```
    pub fn bst2() -> BST<i32> {
        BST::right(1, BST::right(2, BST::leaf(3)))
    }

    /// Creates a tree with other always empty
    /// ```text
    /// 1
    ///  \
    ///   5
    ///  /
    /// 2
    ///  \
    ///   3
    ///    \
    ///     4
    /// ```
    pub fn bst3() -> BST<i32> {
        BST::right(1, BST::left(5, BST::right(2, BST::right(3, BST::leaf(4)))))
    }

    /// Creates a perfect tree
    /// ```text
    ///       .-5-.
    ///      /     \
    ///     3       7
    ///    / \     / \
    ///   2   4   6   8
    /// ```
    pub fn bst4() -> BST<i32> {
        BST::node(
            5,
            BST::node(3, BST::leaf(2), BST::leaf(4)),
            BST::node(7, BST::leaf(6), BST::leaf(8)),
        )
    }

    /// Creates a tree of duplicate values
    /// ```text
    ///        5
    ///       /
    ///      5
    ///     /
    ///    5
    ///   /
    ///  5
    /// ```
    pub fn bst5() -> BST<i32> {
        BST::left(5, BST::left(5, BST::left(5, BST::leaf(5))))
    }

    #[test]
    fn test_helpers() {
        let bst = bst1();
        let empty = Box::new(BST::Empty);
        assert_eq!(
            bst,
            BST::Node(
                3,
                Box::new(BST::Node(
                    2,
                    Box::new(BST::Node(1, empty.clone(), empty.clone())),
                    empty.clone()
                )),
                empty
            )
        );
        bst2();
        bst3();
        bst4();
        bst5();

        println!(
            "You have completed Part 1 of bst. Uncomment `// mod` from the start of this file."
        );
    }
}
