//! # Part 2 - Basic operations
//!
//! - [`BST::is_empty`]
//! - [`BST::root`]
//! - [`BST::into_root`]
//! - [`BST::all`]
//! - [`BST::height`]
//! - [`BST::len`]

// Uncomment the following when you are ready and the tests are passing.
// #[path = "bst3.rs"]
// mod bst3;

use crate::BST;
impl<T: Ord> BST<T> {
    /// Returns whether the tree is [`BST::Empty`].
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// assert!(BST::<()>::Empty.is_empty());
    /// assert!(BST::leaf(1).is_empty().not());
    /// ```
    pub fn is_empty(&self) -> bool {
        self == &Self::Empty
    }

    /// Returns the root element of the (sub)tree, i.e. the value in the node
    /// unless the tree is empty.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// assert_eq!(BST::<()>::Empty.root(), None);
    /// assert_eq!(BST::leaf(2).root(), Some(&2));
    /// assert_eq!(BST::node(2, BST::leaf(1), BST::leaf(3)).root(), Some(&2));
    /// ```
    pub fn root(&self) -> Option<&T> {
        todo!()
    }

    /// Consumes the tree returning the root element as owned unless the tree is
    /// empty.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*;
    /// assert_eq!(BST::<()>::Empty.into_root(), None);
    /// assert_eq!(BST::leaf(2).into_root(), Some(2));
    /// assert_eq!(BST::node(2, BST::leaf(1), BST::leaf(3)).into_root(), Some(2));
    /// ```
    pub fn into_root(self) -> Option<T> {
        todo!()
    }

    /// Returns whether all values match the condition `f`. Scales with `O(n)`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// assert!(BST::leaf(1).all(|num| num == &1));
    /// assert!(BST::right(1, BST::leaf(2)).all(|num| num == &1).not());
    /// ```
    ///
    /// The closure gets never applied if the tree is empty:
    ///
    /// ```
    /// # use week5_bst::*; use std::ops::Not;
    /// assert!(BST::<()>::Empty.all(|_evil| loop { /* never returns muahahaha */ }));
    /// ```
    pub fn all(&self, f: impl Fn(&T) -> bool + Copy) -> bool {
        todo!()
    }

    /// Returns the height of the tree, i.e. the maximum number of consecutive
    /// [`BST::Node`]s. [`BST::Empty`] has zero consecutive nodes.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// assert_eq!(BST::leaf(1).height(), 1);
    /// assert_eq!(BST::right(1, BST::leaf(2)).height(), 2);
    /// ```
    pub fn height(&self) -> usize {
        todo!()
    }

    /// Returns the length of the tree, i.e. the number of [`BST::Node`]s.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use week5_bst::*; use std::ops::Not;
    /// assert_eq!(BST::leaf(1).len(), 1);
    /// assert_eq!(BST::right(1, BST::leaf(2)).len(), 2);
    /// ```
    pub fn len(&self) -> usize {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use std::ops::Not;

    use super::*;
    // Re-export test helpers
    pub use super::super::tests::*;
    #[test]
    fn is_empty() {
        assert!(BST::<()>::Empty.is_empty());

        assert!(bst1().is_empty().not());
        assert!(bst2().is_empty().not());
    }

    #[test]
    fn root() {
        assert_eq!(BST::<()>::Empty.root(), None);

        assert_eq!(
            BST::Node(1, Box::new(BST::Empty), Box::new(BST::Empty)).root(),
            Some(&1)
        );
    }

    #[test]
    fn into_root() {
        assert_eq!(BST::<()>::Empty.into_root(), None);

        assert_eq!(
            BST::Node(1, Box::new(BST::Empty), Box::new(BST::Empty)).into_root(),
            Some(1)
        );
    }

    #[test]
    fn all() {
        let bst = bst1();
        assert!(bst.all(|&val| val < 4));
        assert!(bst.all(|&val| val > 0));

        let bst = bst2();
        assert!(bst.all(|&val| val < 4));
        assert!(bst.all(|&val| val > 0));

        let bst = bst3();
        assert!(bst.all(|&val| val < 6));
        assert!(bst.all(|&val| val > 0));

        let bst = bst4();
        assert!(bst.all(|&val| val < 9));
        assert!(bst.all(|&val| val > 1));
    }

    #[test]
    fn height() {
        assert_eq!(BST::<()>::Empty.height(), 0);
        assert_eq!(bst1().height(), 3);
        assert_eq!(bst2().height(), 3);
        assert_eq!(bst3().height(), 5);
        assert_eq!(bst4().height(), 3);
        assert_eq!(bst5().height(), 4);
    }

    #[test]
    fn len() {
        assert_eq!(BST::<()>::Empty.len(), 0);
        assert_eq!(bst1().len(), 3);
        assert_eq!(bst2().len(), 3);
        assert_eq!(bst3().len(), 5);
        assert_eq!(bst4().len(), 7);
        assert_eq!(bst5().len(), 4);

        println!(
            "You have completed Part 2 of bst. Uncomment `// mod` from the start of this file."
        );
    }
}
