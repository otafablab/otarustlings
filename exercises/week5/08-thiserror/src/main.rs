//! Your task is to implement the conversions to `Error` by deriving
//! `thiserror::Error`. You don't need to add any impls.
//! 
//! - Step one: add `thiserror` as a dependency
//! - Step two: read how to use `thiserror`
//!   https://docs.rs/thiserror/latest/thiserror/ and fix the code.

use std::num::ParseIntError;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Debug, Error)]
enum Error {
    /// A [`CreationError`] from creating the [`PositiveInteger`].
    #[error("creating PositiveInteger failed: {0}")]
    Creation(CreationError),
    /// A [`ParseIntError`] from parsing the input string.
    ParseInt(ParseIntError),
}

#[derive(PartialEq, Debug)]
enum CreationError {
    Negative,
    Zero,
}

// Don't change anything below this line.

impl FromStr for PositiveNonzeroInteger {
    type Err = Error;
    fn from_str(s: &str) -> Result<PositiveNonzeroInteger, Self::Err> {
        let x: i64 = s.parse()?;
        Ok(PositiveNonzeroInteger::new(x)?)
    }
}

#[derive(PartialEq, Debug)]
struct PositiveNonzeroInteger(u64);

impl PositiveNonzeroInteger {
    fn new(value: i64) -> Result<PositiveNonzeroInteger, CreationError> {
        match value {
            0 => Err(CreationError::Zero),
            1.. => Ok(PositiveNonzeroInteger(value as u64)),
            _ => Err(CreationError::Negative),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_parse_error() {
        let res = PositiveNonzeroInteger::from_str("not a number");
        assert!(matches!(
            res,
            Err(Error::ParseInt(_))
        ));
        assert_eq!(
            format!("{}", res.unwrap_err()),
            "parsing failed: invalid digit found in string"
        );
    }
    #[test]
    fn test_negative() {
        let res = PositiveNonzeroInteger::from_str("-555");
        assert_eq!(
            res,
            Err(Error::Creation(CreationError::Negative))
        );
        assert_eq!(
            format!("{}", res.unwrap_err()),
            "creating PositiveInteger failed: number is negative"
        );
    }
    #[test]
    fn test_zero() {
        let res = PositiveNonzeroInteger::from_str("0");
        assert_eq!(
            res,
            Err(Error::Creation(CreationError::Zero))
        );
        assert_eq!(
            format!("{}", res.unwrap_err()),
            "creating PositiveInteger failed: number is zero"
        );
    }
    #[test]
    fn test_positive() {
        let x = PositiveNonzeroInteger::new(42);
        assert!(x.is_ok());
        assert_eq!(PositiveNonzeroInteger::from_str("42").unwrap(), x.unwrap());
    }
}
