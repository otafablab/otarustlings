//! Your task is to fill in the blanks. Read the documentation comments.
//!
//! Using catch-all error types like `Box<dyn error::Error>` isn't recommended
//! for library code, where callers might want to handle the errors. It is
//! possible to downcast the trait object, but is more cumbersome than matching
//! on an enum directly.

use std::num::ParseIntError;

#[derive(PartialEq, Debug)]
enum Error {
    /// A [`CreationError`] from creating the [`PositiveInteger`].
    Creation(___),
    /// A [`ParseIntError`] from parsing the input string.
    ParseInt(___),
}

/// Parses a string into a [`PositiveInteger`].
///
/// # Errors
///
/// - [`Error::ParseInt`] is returned if parsing `s` into an integer fails.
/// - [`Error::Creation`] is returned if creating the [`PositiveInteger`] fails.
fn parse_positive(s: impl AsRef<str>) -> Result<PositiveInteger, Error> {
    // Tip: Error::ParseInt is also a function: ___ -> Error
    let x: i64 = s.as_ref().parse().map_err(___)?;
    PositiveInteger::new(x).___
}

// Don't change anything below this line.

#[derive(PartialEq, Debug)]
struct PositiveInteger(u64);

#[derive(PartialEq, Debug)]
enum CreationError {
    Negative,
    Zero,
}

impl PositiveInteger {
    fn new(value: i64) -> Result<PositiveInteger, CreationError> {
        match value {
            0 => Err(CreationError::Zero),
            1.. => Ok(PositiveInteger(value as u64)),
            _ => Err(CreationError::Negative),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_parse_error() {
        assert!(matches!(
            parse_positive("not a number"),
            Err(Error::ParseInt(_))
        ));
    }
    #[test]
    fn test_negative() {
        assert_eq!(
            parse_positive("-555"),
            Err(Error::Creation(CreationError::Negative))
        );
    }
    #[test]
    fn test_zero() {
        assert_eq!(
            parse_positive("0"),
            Err(Error::Creation(CreationError::Zero))
        );
    }
    #[test]
    fn test_positive() {
        let x = PositiveInteger::new(42);
        assert!(x.is_ok());
        assert_eq!(parse_positive("42"), Ok(x.unwrap()));
    }
}
