//! Your task is to fix the return type of `main` and change the "user input" 
//! if you know what I mean...

use std::error;
use std::fmt;
use std::num::ParseIntError;

#[test]
fn main() -> Result<(), ParseIntError> {
    let pretend_user_input = "-42";

    let x: i64 = pretend_user_input.parse()?;
    let s = PositiveNonzeroInteger::new(x)?;

    assert_eq!(PositiveNonzeroInteger(42), s);
    Ok(())
}

// Don't change anything below this line.

#[derive(PartialEq, Debug)]
struct PositiveNonzeroInteger(u64);

#[derive(PartialEq, Debug)]
enum CreationError {
    Negative,
    Zero,
}

impl PositiveNonzeroInteger {
    fn new(value: i64) -> Result<PositiveNonzeroInteger, CreationError> {
        match value {
            0 => Err(CreationError::Zero),
            1.. => Ok(PositiveNonzeroInteger(value as u64)),
            _ => Err(CreationError::Negative),
        }
    }
}

// This is required so that `CreationError` can implement `error::Error`.
impl fmt::Display for CreationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let description = match *self {
            CreationError::Negative => "number is negative",
            CreationError::Zero => "number is zero",
        };
        f.write_str(description)
    }
}

impl error::Error for CreationError {}
