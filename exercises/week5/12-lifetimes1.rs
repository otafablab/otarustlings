//! Your task is to fix the lifetime errors. Read the chapter about lifetimes:
//! https://doc.rust-lang.org/stable/book/ch10-03-lifetime-syntax.html

fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

fn always_first(x: &str, y: &str) -> &str {
    x
}

// Do not edit anything below

#[test]
fn main() {
    let s1 = "static string literal";
    {
        let s2 = "not static".to_string();
        let longest = longest(s1, &s2);

        assert_eq!(longest, "static string literal");
    };
    // longest could not live here as it cannot outlive s2 regardless of which
    // string is longer

    let first = {
        let s2 = "not static".to_string();
        always_first(s1, &s2)
    };

    // first should be able to live here as it ignores the lifetime of the
    // second argument
    assert_eq!(first, "static string literal");
}
