//! Your task is to define the struct Semver and implement PartialEq and
//! PartialOrd for it. You also need to implement FromStr for Semver for
//! convenience.

use std::cmp::Ordering;
use std::num::ParseIntError;
use std::str::FromStr;

// You are not allowed to derive more traits
#[derive(Debug)]
struct Semver {
    major: u32,
    minor: u32,
    patch: u32,
}

impl PartialEq for Semver {
    fn eq(&self, other: &Self) -> bool {
        // TODO
    }
}

// TODO

impl Semver {
    pub fn new(major: u32, minor: u32, patch: u32) -> Self {
        // TODO
    }

    // TODO is_stable, breaking, bump
}

impl FromStr for Semver {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // TODO
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_str() {
        let error = "a.0.0".parse::<Semver>().unwrap_err();

        assert_eq!(error.kind(), &std::num::IntErrorKind::InvalidDigit);

        let error = "0.1.c".parse::<Semver>().unwrap_err();

        assert_eq!(error.kind(), &std::num::IntErrorKind::InvalidDigit);

        let error = "0.1.4294967296".parse::<Semver>().unwrap_err();

        assert_eq!(error.kind(), &std::num::IntErrorKind::PosOverflow);
    }

    #[test]
    fn is_stable() {
        let version1: Semver = "1.61.0".parse().unwrap();
        let version2: Semver = "0.10.5".parse().unwrap();

        assert!(version1.is_stable());
        assert!(!version2.is_stable());
    }

    #[test]
    fn major_comparison() {
        let version1: Semver = "1.61.0".parse().unwrap();
        let version2: Semver = "2.0.5".parse().unwrap();

        assert!(version1 < version2);
    }

    #[test]
    fn minor_comparison() {
        let version1: Semver = "1.0.5".parse().unwrap();
        let version2: Semver = "1.61.0".parse().unwrap();

        assert!(version1 < version2);
    }

    #[test]
    fn patch_comparison() {
        let version1: Semver = "1.9.5".parse().unwrap();
        let version2: Semver = "1.9.15".parse().unwrap();

        assert!(version1 < version2);
    }

    #[test]
    fn breaking_change() {
        let version: Semver = "1.61.0".parse().unwrap();
        let next = version.breaking();

        assert_eq!(next, "2.0.0".parse().unwrap());

        let version: Semver = "0.2.0".parse().unwrap();
        let next = version.breaking();

        assert_eq!(next, "0.3.0".parse().unwrap());
    }

    #[test]
    fn minor_bump() {
        let version: Semver = "1.61.0".parse().unwrap();
        let next = version.bump();

        assert_eq!(next, "1.62.0".parse().unwrap());

        let version: Semver = "0.2.0".parse().unwrap();
        let next = version.bump();

        assert_eq!(next, "0.2.1".parse().unwrap());
    }
}
