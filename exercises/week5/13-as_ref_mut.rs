//! Your task is to change the functions `byte_counter` and
//! `capitalize_first_word` to support different types of (be generic over)
//! references. More specifically you need to support a `&String` in place of a
//! `&str` and a `&mut String` in place of a `&mut str`.
//!
//! The traits AsRef and AsMut allow for cheap reference-to-reference (compared
//! to From/Into value-to-value) conversions. Read more about them at
//! - https://doc.rust-lang.org/std/convert/trait.AsRef.html
//! - https://doc.rust-lang.org/std/convert/trait.AsMut.html

/// Obtain the number of bytes (not characters) in the given argument
fn byte_counter(s: &String) -> usize {
    s.as_bytes().len()
}

/// Capitalize first word in a sentence
fn capitalize_first_word(mut s: &mut String) {
    let i = s.find(' ').unwrap_or(s.len());
    s[..i].make_ascii_uppercase();
}

// Don't change the tests!
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bytes() {
        // é takes two bytes
        let s = "Café au lait";
        assert_eq!(byte_counter(s), 13);

        let s = s.to_string();
        assert_eq!(byte_counter(s), 13);
    }

    #[test]
    fn capitalize_word() {
        let mut s = "word".to_string();
        capitalize_first_word(&mut s);
        assert_eq!(s, "WORD");
    }

    #[test]
    fn capitalize_ascii() {
        let mut s = "Cafe au lait".to_string();
        let sref: &mut str = s.as_mut_str();
        capitalize_first_word(sref);
        assert_eq!(s, "CAFE au lait");
    }

    #[test]
    fn capitalize_ascii_only() {
        let mut s = "Caffè latte".to_string();
        capitalize_first_word(&mut s);
        assert_eq!(s, "CAFFè latte");
    }
}
