//! # Week 1 quiz
//!
//! Type the number of the best choice in each constant in place of _

/// Question 1: Which of the following commands creates a new crate:
/// 1. cargo create
/// 2. cargo new
/// 3. cargo crate new
/// 4. cargo crate create
const ANSWER1: i32 = _;

/// Question 2: After creating a new crate, what does the default code do:
/// 1. Prints "Hello, world!"
/// 2. Nothing
/// 3. Launches a number guessing game
/// 4. Launches an AI chatbot
const ANSWER2: i32 = _;

// Question 3: What does the following code do (assuming rand is installed)
// ```
// fn main() {
//     let n = rand::thread_rng().gen_range(1..101);
//     println!("{}", n);
// }
// ```
/// 1. Asks for user input and prints it
/// 2. Prints a random string
/// 3. Fails to compile
/// 4. Prints a random number
const ANSWER3: i32 = _;

// Question 4: Why will this code fail to compile?
// ```
// fn main() {
//     let x: i32 = "Hello, world!";
// }
// ```
/// 1. Division by zero
/// 2. Type mismatch
/// 3. Missing crates
/// 4. Out of memory
const ANSWER4: i32 = _;

// Don't edit the following code

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::read_to_string;

    #[test]
    fn question1() {
        let correct_answers =
            read_to_string("exercises/week1/01-quiz.answers").expect("Unable to open answers");
        let your_answer = (0x30 + ANSWER1 as u8) as char;
        assert_eq!(
            your_answer,
            correct_answers.chars().nth(0).unwrap(),
            "Question 1 incorrect"
        );
    }

    #[test]
    fn question2() {
        let correct_answers =
            read_to_string("exercises/week1/01-quiz.answers").expect("Unable to open answers");
        let your_answer = (0x30 + ANSWER2 as u8) as char;
        assert_eq!(
            your_answer,
            correct_answers.chars().nth(1).unwrap(),
            "Question 2 incorrect"
        );
    }

    #[test]
    fn question3() {
        let correct_answers =
            read_to_string("exercises/week1/01-quiz.answers").expect("Unable to open answers");
        let your_answer = (0x30 + ANSWER3 as u8) as char;
        assert_eq!(
            your_answer,
            correct_answers.chars().nth(2).unwrap(),
            "Question 3 incorrect"
        );
    }

    #[test]
    fn question4() {
        let correct_answers =
            read_to_string("exercises/week1/01-quiz.answers").expect("Unable to open answers");
        let your_answer = (0x30 + ANSWER4 as u8) as char;
        assert_eq!(
            your_answer,
            correct_answers.chars().nth(3).unwrap(),
            "Question 4 incorrect"
        );
    }
}
