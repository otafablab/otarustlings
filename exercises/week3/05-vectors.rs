//! Step 1: fix `vec_loop` function's definition to allow using
//! `v.iter_mut()`.
//!
//! Step 2: complete the loop so that each number in the Vec is
//! multiplied by 2.

fn vec_loop(v: Vec<i32>) -> Vec<i32> {
    for elem in v.iter_mut() {
        // TODO: Fill this up so that each element in the Vec `v` is
        // multiplied by 2.  Hint: mutating the referent of `&mut i32`
        // requires a dereference.
    }

    v
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec_loop() {
        let v: Vec<i32> = (1..).filter(|x| x % 2 == 0).take(5).collect();
        let ans = vec_loop(v.clone());

        assert_eq!(ans, v.iter().map(|x| x * 2).collect::<Vec<i32>>());
    }
}
