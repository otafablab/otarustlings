//! Mutate the tuple so that the test passes

fn swap_tuple((x, y): (i32, i32)) -> (i32, i32) {
    (y, x)
}

#[test]
fn main() {
    let mut tup = (2, 5);

    // TODO mutate the second element in `tup`

    assert_eq!(swap_tuple(tup), (4, 2));
}
