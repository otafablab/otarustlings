//! Destructure the `cat` tuple so that the println will work.

#[test]
fn main() {
    let cat = ("Furry McFurson", 3.5);
    let ___ = cat;

    println!("{} is {} years old.", name, age); // Don't change this line
}
