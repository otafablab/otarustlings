//! Ok, here are a bunch of values-- some are `String`s, some are string slices:
//! `&str`s. Your task is to call one of these two functions on each value
//! depending on what you think each value is. That is, add either `string_slice`
//! or `string` before the parentheses on each line. There may be multiple
//! solutions.

fn string_slice(arg: &str) {
    println!("{}", arg);
}
fn string(arg: String) {
    println!("{}", arg);
}

fn main() {
    ____("blue");
    ____("red".to_string());
    ____(String::from("hi"));
    ____("rust is fun!".to_owned());
    ____("nice weather".into());
    ____(format!("Interpolation {}", "Station"));
    ____(&String::from("abc")[0..1]);
    ____("  hello there ".trim());
    ____("Happy Monday!".to_string().replace("Mon", "Tues"));
    ____("mY sHiFt KeY iS sTiCkY".to_lowercase());
}
