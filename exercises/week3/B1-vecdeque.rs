//! This is a bonus exercise.
//! 
//! Your task is to find the right number to rotate the VecDeque by. You are
//! only allowed to change the argument to `rotate_right`

use std::collections::VecDeque;

fn make_uncontiguous_vecdeq() -> VecDeque<i32> {
    let mut vd: VecDeque<_> = (1..=5).chain(1..=5).collect();

    // TODO rotate the deque until both slices are equal
    vd.rotate_right(1);

    vd
}

#[test]
fn main() {
    let vd = make_uncontiguous_vecdeq();

    let (head, tail) = vd.as_slices();

    assert_eq!(head, tail);
}