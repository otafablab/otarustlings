//! Your task is to mutate the point in main to pass the assertion

/// A struct is a statically sized collection of data with keys and values.
struct Point {
    x: i32,
    y: i32,
}

fn display_point(point: &Point) {
    println!("{}, {}", point.x, point.y);
}

#[test]
fn main() {
    let mut point = Point {
        x: 3,
        y: -5,
    };

    // TODO mutate point's x-coordinate

    display_point(&point);
    assert_eq!(point.x, 6);
}
