//! Note: if your editor complains about `try_into`, it only works on
//! edition 2021 which `otarustlings` supports, so don't mind the
//! error.

fn make_mult_table(num: i32) -> [String; 10] {
    // The _ here tells Rust that it needs to figure it out so that
    // you don't need to. The Vec is required as collect can return
    // any collection that implements the FromIterator trait.
    let vec: Vec<_> = (0..10)
        .into_iter()
        .map(|i| format!("{i} * {num} = {}", i as i32 * num))
        .collect();

    vec.try_into().expect("vector to have exactly 10 elements")
}

#[test]
fn main() {
    let a = make_mult_table(3);

    // TODO create a reference to the correct element
    let nine = ___;

    println!("the array is {:#?}", a);
    assert_eq!(nine, "3 * 3 = 9");
}
