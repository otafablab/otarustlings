//! Your task is to fix the function `third`.  Hint: the issue is not
//! related to lifetimes.
//!
//! Note: The exercises use strings in array because they are not
//! cheap to copy (unlike integers for example).

// TODO fix "lifetime" errors
fn third(a: [String]) -> (&[String], &str) {
    (a, a[2])
}

// Don't edit the following code

fn eat(s: String) {
    println!("{}", s);
}

#[test]
fn main() {
    let a = [
        "first".to_string(),
        "second".to_string(),
        "third".to_string(),
        "fourth".to_string(),
    ];

    let (b, third) = third(&a);

    println!("{}", third);

    eat(a
        .into_iter()
        .nth(1)
        .expect("a to contain at least two elements"));
}
