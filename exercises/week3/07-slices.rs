//! Your task is to create a `Vec` which holds the exact same elements as in the
//! array `a`. Make me compile and pass the test! Use the `vec!` macro:
//! https://doc.rust-lang.org/std/macro.vec.html

fn array_and_vec() -> ([i32; 4], Vec<i32>) {
    let a = [10, 20, 30, 40];
    let v = ___;

    (a, v)
}

#[test]
fn test_array_and_vec_similarity() {
    let (a, v) = array_and_vec();
    assert_eq!(a, v[..]);
}