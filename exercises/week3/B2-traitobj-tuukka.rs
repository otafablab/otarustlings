//! This is a bonus exercise (hard)

use std::collections::HashMap;

fn make_vec() -> Vec<(&'static str, i32)> {
    vec![("a",1),("b",2),("c",3)]
}

fn make_hashmap() -> HashMap<&'static str, i32> {
    let mut hm = HashMap::new();
    hm.insert("d", 4);
    hm.insert("e", 5);
    hm.insert("f", 6);
    hm
}

// TODO fill in the blanks
fn print_dyn_iter(iter: &mut dyn ___) {
    for (key, value) in iter {
        println!("{key} - {value}")
    }
}

#[test]
fn main() {
    let mut v = make_vec().into_iter();
    let mut hm = make_hashmap().into_iter();

    print_dyn_iter(&mut v);
    assert_eq!(v.next(), None);

    print_dyn_iter(&mut hm);
    assert_eq!(hm.next(), None);
}