//! Your task is to capitalize the first letter/character in `string`. Search
//! the Rust standard library documentation for `str::chars`. Note that a String
//! has all the methods of str because of the `Deref` trait. Don't be afraid to
//! search more information online

fn capitalize_first(string: &mut String) {
    // TODO
}

#[test]
fn letters() {
    let mut string = "testing".to_string();

    capitalize_first(&mut string);

    assert_eq!(string, "Testing");
}

#[test]
fn numbers() {
    let mut string = "1234".to_string();

    capitalize_first(&mut string);

    assert_eq!(string, "1234");
}

#[test]
fn special() {
    let mut string = "山".to_string();

    capitalize_first(&mut string);

    assert_eq!(string, "山");
}

#[test]
fn emoji() {
    let mut string = "❤".to_string();

    capitalize_first(&mut string);

    assert_eq!(string, "❤");
}

#[test]
fn sigma() {
    let mut string = "Σ".to_string();

    capitalize_first(&mut string);

    assert_eq!(string, "Σ");
}

// The hard one
#[test]
fn ligatures() {
    let mut string = "ﬁrm".to_string();

    capitalize_first(&mut string);

    assert_eq!(string, "Firm");
}
