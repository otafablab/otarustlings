//! You can't change anything except adding or removing references `&`

#[test]
fn main() {
    let data = "Rust is great!".to_string();

    get_char(data);

    string_uppercase(&data);
}

// TODO Should not take ownership
fn get_char(data: String) -> char {
    data.chars().last().unwrap()
}

// TODO Should take ownership
fn string_uppercase(mut data: &String) {
    data = &data.to_uppercase();

    println!("{}", data);
}
