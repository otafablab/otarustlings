//! This is a bonus exercise (hard)
//! 
//! Your task is to make the main function not panic. You can only change code
//! inside main.
//! 
//! Part 1: swap the values of v and hm safely with something from
//! https://doc.rust-lang.org/std/mem/index.html
//! 
//! Part 2: fix the next problem ¯\_(°ペ)_/¯

use std::any::Any;
use std::collections::HashMap;

fn make_something() -> Box<dyn Any> {
    Box::new(vec![("a", 1), ("b", 2), ("c", 3)])
}

fn make_something_else() -> Box<dyn Any> {
    let mut hm = HashMap::new();
    hm.insert("d", 4);
    hm.insert("e", 5);
    hm.insert("f", 6);
    Box::new(hm)
}

fn check_type_vec(any: &mut dyn Any) {
    if let Some(vec) = any.downcast_ref::<Vec<(&str, i32)>>() {
        println!("is vector");
    } else {
        panic!("TypeError: Vec expected");
    }
}

fn check_type_hashmap(any: &mut dyn Any) {
    if let Some(hm) = any.downcast_ref::<HashMap<&str, i32>>() {
        println!("is hashmap");
    } else {
        panic!("TypeError: HashMap expected");
    }
}

// You can only edit code in the main function
#[test]
fn main() {
    let mut v: Box<dyn Any> = make_something();
    let mut hm: Box<dyn Any> = make_something_else();

    check_type_hashmap(&mut v);
    check_type_vec(&mut hm);
}
