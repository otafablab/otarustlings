//! Your task is to take a nice slice into `a` so that the test passes.

#[test]
fn slice_out_of_array() {
    let a = [1, 2, 3, 4, 5];

    let nice_slice = ___;

    assert_eq!([2, 3, 4], nice_slice)
}
