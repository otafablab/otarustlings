//! This file is used to get syntax highlighting and rust-analyzer to work in
//! the individual exercises.

mod week1 {
    #[path = "01-quiz.rs"]
    mod quiz;
}

mod week2 {
    #[path = "bonus-ownership.rs"]
    mod bonus_ownership;
    #[path = "05-functions.rs"]
    mod functions;
    #[path = "06-if.rs"]
    mod if1;
    #[path = "07-if.rs"]
    mod if2;
    #[path = "08-ownership.rs"]
    mod ownership1;
    #[path = "09-ownership.rs"]
    mod ownership2;
    #[path = "04-variables.rs"]
    mod variables;
    #[path = "01-variables.rs"]
    mod variables1;
    #[path = "02-variables.rs"]
    mod variables2;
    #[path = "03-variables.rs"]
    mod variables3;
}

mod week3 {
    #[path = "B2-traitobj-tuukka.rs"]
    mod bonus_traitobj_tuukka1;
    #[path = "B3-traitobj-tuukka.rs"]
    mod bonus_traitobj_tuukka2;
    #[path = "B1-vecdeque.rs"]
    mod bonus_vecdeque;
    #[path = "09-borrowing.rs"]
    mod borrowing1;
    #[path = "10-borrowing.rs"]
    mod borrowing2;
    #[path = "06-slices.rs"]
    mod slices1;
    #[path = "07-slices.rs"]
    mod slices2;
    #[path = "08-slices.rs"]
    mod slices3;
    #[path = "04-strings.rs"]
    mod strings1;
    #[path = "12-strings.rs"]
    mod strings2;
    #[path = "11-structs.rs"]
    mod structs;
    #[path = "01-tuples.rs"]
    mod tuples1;
    #[path = "02-tuples.rs"]
    mod tuples2;
    #[path = "03-tuples.rs"]
    mod tuples3;
    #[path = "05-vectors.rs"]
    mod vectors;
}

mod week4 {
    #[path = "B1-lifetimes.rs"]
    mod bonus_lifetimes;
    #[path = "05-derive.rs"]
    mod derive;
    #[path = "01-enums.rs"]
    mod enums;
    #[path = "03-enums2.rs"]
    mod enums2;
    #[path = "08-if-let.rs"]
    mod if_let;
    #[path = "07-match.rs"]
    mod r#match;
    #[path = "09-match2.rs"]
    mod match2;
    #[path = "04-newtype.rs"]
    mod newtype;
    #[path = "10-option.rs"]
    mod option;
    #[path = "02-structs.rs"]
    mod structs;
    #[path = "06-type-safety.rs"]
    mod type_safety;
    // B2 is crate-like
    #[path = "B3-type-safety-generics.rs"]
    mod bonus_type_safety_generics;
    // B4 is crate-like
    #[path = "B6-closure-traits.rs"]
    mod bonus_closure_traits;
    #[path = "B5-closures.rs"]
    mod bonus_closures;
}

mod week5 {
    #[path = "13-as_ref_mut.rs"]
    mod as_ref_mut;
    #[path = "B1-closures.rs"]
    mod bonus_closures;
    #[path = "B2-lifetimes2.rs"]
    mod bonus_lifetimes2;
    #[path = "02-errors.rs"]
    mod errors;
    #[path = "03-errors2.rs"]
    mod errors2;
    #[path = "05-errors3.rs"]
    mod errors3;
    #[path = "07-errors4.rs"]
    mod errors4;
    #[path = "06-from-into.rs"]
    mod from_into;
    #[path = "01-impl.rs"]
    mod r#impl;
    #[path = "12-lifetimes1.rs"]
    mod lifetimes1;
    #[path = "04-monad.rs"]
    mod monad;
    #[path = "11-recursive_types.rs"]
    mod recursive_types;
    #[path = "09-semver.rs"]
    mod semver;
    #[path = "10-traits.rs"]
    mod traits;
}
