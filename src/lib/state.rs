use crate::menu::Status;
use crate::utils::list_exercises;
use chrono::{DateTime, Utc};
use itertools::{GroupBy, Itertools};
use serde::{Deserialize, Serialize};
use std::collections::{btree_set, BTreeMap, BTreeSet};
use std::default::Default;
use std::fs::{read_to_string, File};
use std::io::{self, ErrorKind, Write};
use std::mem::swap;
use std::path::{Path, PathBuf};
use tracing::{debug, trace};

/// `Exercise` represents an exercise.
///
/// TODO Fix implicit equality and ordering of exercises
#[derive(Debug, Clone)]
pub struct Exercise {
    pub path: PathBuf,
    pub exercise_state: ExerciseState,
}

impl PartialEq for Exercise {
    fn eq(&self, other: &Self) -> bool {
        self.path == other.path
    }
}

impl Eq for Exercise {}

impl PartialOrd for Exercise {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.path.partial_cmp(&other.path)
    }
}

impl Ord for Exercise {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.path.cmp(&other.path)
    }
}

impl From<(PathBuf, ExerciseState)> for Exercise {
    fn from(from: (PathBuf, ExerciseState)) -> Self {
        Self {
            path: from.0,
            exercise_state: from.1,
        }
    }
}

impl Exercise {
    pub fn file_name(&self) -> &str {
        self.path.file_name().unwrap().to_str().unwrap()
    }

    pub fn folder_name(&self) -> &str {
        self.path
            .parent()
            .unwrap()
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
    }

    pub fn status(&self) -> Status {
        match (self.exercise_state.started, self.exercise_state.completed) {
            (_, Some(_)) => Status::Completed,
            (Some(_), None) => Status::Started,
            (_, _) => Status::NotStarted,
        }
    }
}

#[derive(
    Copy, Clone, Serialize, Deserialize, Hash, Default, Debug, PartialEq, Eq, PartialOrd, Ord,
)]
pub struct ExerciseState {
    pub started: Option<DateTime<Utc>>,
    pub completed: Option<DateTime<Utc>>,
}

impl ExerciseState {
    pub fn start_exercise(&mut self) -> DateTime<Utc> {
        if let Some(date) = self.started {
            date
        } else {
            let now = Utc::now();
            self.started = Some(now);
            now
        }
    }

    pub fn complete_exercise(&mut self) -> DateTime<Utc> {
        if let Some(date) = self.completed {
            date
        } else {
            let now = Utc::now();
            self.completed = Some(now);
            now
        }
    }
}

/// State of the exercises.
#[derive(Debug, Default, Clone)]
pub struct State(pub BTreeSet<Exercise>);

impl From<BTreeMap<PathBuf, ExerciseState>> for State {
    fn from(map: BTreeMap<PathBuf, ExerciseState>) -> Self {
        Self(map.into_iter().map(Exercise::from).collect())
    }
}

impl State {
    /// Creates a new empty state.
    pub fn new() -> Self {
        Self::default()
    }

    /// Gets the exercise
    pub fn get(&self, path: impl AsRef<Path>) -> Option<&Exercise> {
        self.0.iter().find(|e| e.path == path.as_ref())
    }

    /// Collects all exercises under `path` as their own default [`ExerciseState`].
    ///
    /// If the folder at `path` is empty, the state will be empty too. TODO Should error.
    pub fn scan(path: impl AsRef<Path>) -> Self {
        debug!("scanning {:?}", path.as_ref());
        let exercises = list_exercises(path)
            .map(|path| (path, ExerciseState::default()).into())
            .collect();
        Self(exercises)
    }

    /// Tries to deserialize `exercises/state.toml` as [`Self`].
    ///
    /// # Errors
    ///
    /// Returns an [`io::Error`] if reading the state file fails or if
    /// the file contains invalid toml.
    pub fn open(path: impl AsRef<Path>) -> io::Result<Self> {
        debug!("opening state file in {:?}", path.as_ref());
        let map: BTreeMap<PathBuf, ExerciseState> =
            toml::from_str(&read_to_string(path.as_ref().join("state.toml"))?)?;
        Ok(map.into())
    }

    /// Reads `exercises/state.toml` if it exists and scans the.
    /// `exercises` directory extending the state from.
    /// `state.toml`. This is the logical option in most cases.
    ///
    /// # Errors
    ///
    /// Same errors as [`Self::open`].
    pub fn scan_open_default(path: impl AsRef<Path>) -> io::Result<Self> {
        let path = path.as_ref();
        let mut state = match Self::open(path) {
            Err(e) => {
                if e.kind() == ErrorKind::NotFound {
                    debug!("no previous state file found");
                    Self::default()
                } else {
                    return Err(e);
                }
            }
            Ok(state) => {
                debug!("previous state file found");
                state
            }
        };
        let other = Self::scan(path);
        trace!("{:?}", state);
        trace!("{:?}", other);
        state.extend(other);
        Ok(state)
    }

    /// Saves the state to `exercises/state.toml`.
    ///
    /// # Errors
    ///
    /// Returns an [`io::Error`] if creating or writing to the state
    /// file fails.
    pub fn save(&self) -> io::Result<()> {
        let mut file = File::create("exercises/state.toml")?;
        let mut map = BTreeMap::new();

        for ex in &self.0 {
            map.insert(&ex.path, &ex.exercise_state);
        }
        debug!("writing state to exercises/state.toml");
        file.write_all(
            toml::to_string(&map)
                .expect("state to be serializable")
                .as_bytes(),
        )?;
        Ok(())
    }

    /// Joins two states favoring self's exercises. Useful for adding
    /// missing exercises with:
    ///
    /// ```
    /// # use otarustlings::state::{State, Exercise, ExerciseState};
    /// let mut left = State::new();
    /// # left.0.insert(Exercise { path: "old_week1/ex1.rs".into(), exercise_state: ExerciseState::default() });
    /// left.extend(State::scan("exercises"));
    /// ```
    ///
    /// # Example
    ///
    /// ```
    /// # use chrono::Utc;
    /// # use otarustlings::state::{State, Exercise, ExerciseState};
    /// let started = ExerciseState {
    ///     started: Some(Utc::now()),
    ///     ..ExerciseState::default()
    /// };
    /// // left:
    /// //   week1
    /// //     ex1.rs (started)
    /// //   week2
    /// //     ex1.rs (default)
    /// //
    /// // right:
    /// //   week1
    /// //     ex1.rs (default)
    /// //   week3
    /// //     ex1.rs (default)
    ///
    /// let mut left = State::new();
    /// left.0.insert(Exercise { path: "week1/ex1.rs".into(), exercise_state: started.clone() });
    /// left.0.insert(Exercise { path: "week2/ex1.rs".into(), exercise_state: ExerciseState::default() });
    ///
    /// let mut right = State::new();
    /// right.0.insert(Exercise { path: "week1/ex1.rs".into(), exercise_state: ExerciseState::default() });
    /// right.0.insert(Exercise { path: "week3/ex1.rs".into(), exercise_state: ExerciseState::default() });
    ///
    /// // Extending `left` with `right` should give
    /// // left:
    /// //   week1
    /// //     ex1.rs (started)
    /// //   week2
    /// //     ex1.rs (default)
    /// //   week3
    /// //     ex1.rs (default)
    ///
    /// left.extend(right);
    ///
    /// assert_eq!(left.0.iter().nth(0).unwrap().exercise_state, started.clone());
    /// ```
    pub fn extend(&mut self, mut other: Self) {
        swap(&mut self.0, &mut other.0); // swapping sets inverts the priority
        self.0.append(&mut other.0);
    }

    /// Returns an iterator yielding [`Exercise`]s.
    pub fn iter(&self) -> impl Iterator<Item = &'_ Exercise> {
        self.0.iter()
    }

    /// Returns an iterator yielding a tuple of folder and the exercises within.
    pub fn group_by_folder<'s>(
        &'s self,
    ) -> GroupBy<&'_ str, btree_set::Iter<'_, Exercise>, impl FnMut(&'_ &'s Exercise) -> &'s str>
    {
        self.0.iter().group_by(|t| t.folder_name())
    }

    /// Returns the next exercise of the week or `None` if the exercise is the last one in the folder.
    ///
    /// # Examples
    ///
    /// Get the next exercise `ex2` when `ex1` comes before `ex2` in `week1`:
    ///
    /// ```
    /// # use chrono::Utc;
    /// # use otarustlings::state::{State, Exercise, ExerciseState};
    /// let started = ExerciseState {
    ///     started: Some(Utc::now()),
    ///     ..ExerciseState::default()
    /// };
    ///
    /// let ex1 = Exercise { path: "week1/ex1.rs".into(), exercise_state: started.clone() };
    /// let ex2 = Exercise { path: "week1/ex2.rs".into(), exercise_state: ExerciseState::default() };
    /// let mut state = State::new();
    /// state.0.insert(ex1.clone());
    /// state.0.insert(ex2.clone());
    ///
    /// assert_eq!(state.get_next_exercise_of_week(&ex1), Some(&ex2));
    /// ```
    ///
    /// Should return `None` if the exercise is last exercise of the week.
    ///
    /// ```
    /// # use chrono::Utc;
    /// # use otarustlings::state::{State, Exercise, ExerciseState};
    /// let started = ExerciseState {
    ///     started: Some(Utc::now()),
    ///     ..ExerciseState::default()
    /// };
    ///
    /// let w1ex1 = Exercise { path: "week1/ex1.rs".into(), exercise_state: started.clone() };
    /// let w2ex1 = Exercise { path: "week2/ex1.rs".into(), exercise_state: ExerciseState::default() };
    /// let mut state = State::new();
    /// state.0.insert(w1ex1.clone());
    /// state.0.insert(w2ex1.clone());
    ///
    /// assert_eq!(state.get_next_exercise_of_week(&w1ex1), None);
    /// ```
    pub fn get_next_exercise_of_week(&self, exercise: &Exercise) -> Option<&Exercise> {
        let mut exercises = self.0.iter();
        exercises.find(|&x| x == exercise);
        // Return None if the next exercise does not exist
        let not_last_exercise = exercises.next()?;
        // Return None if the next exercise is not in the same folder
        if not_last_exercise.folder_name() == exercise.folder_name() {
            Some(not_last_exercise)
        } else {
            None
        }
    }

    pub fn next_exercise(&self, exercise: &Exercise) -> &Exercise {
        let mut exercises = self.0.iter();

        exercises.find(|&x| x == exercise);
        exercises
            .next()
            .unwrap_or_else(|| self.0.iter().next().unwrap())
    }

    pub fn prev_exercise(&self, exercise: &Exercise) -> &Exercise {
        let mut exercises = self.0.iter().rev();

        exercises.find(|&x| x == exercise);
        exercises
            .next()
            .unwrap_or_else(|| self.0.iter().last().unwrap())
    }

    pub fn next_week_exercise(&self, exercise: &Exercise) -> Option<&Exercise> {
        let mut exercises = self.0.iter();
        // Walk the iterator until `exercise`
        exercises.find(|&x| x == exercise);
        // Walk the iterator until the week/folder_name is different
        exercises.find(|&x| x.folder_name() != exercise.folder_name())
    }

    pub fn prev_week_exercise(&self, exercise: &Exercise) -> Option<&Exercise> {
        let mut exercises = self.0.iter().rev();
        // Walk the iterator until `exercise`
        exercises.find(|&x| x == exercise);
        // Walk the iterator until the week/folder_name is different
        exercises.find(|&x| x.folder_name() != exercise.folder_name())
    }

    pub fn exercise_index(&self, exercise: &Exercise) -> usize {
        let (index, _) = self
            .0
            .iter()
            .enumerate()
            .find(|(_i, x)| x == &exercise)
            .expect("exercise to be in the state");
        index
    }

    pub fn first_unfinished(&self) -> Option<&Exercise> {
        self.0
            .iter()
            .find(|x| matches!(x.status(), Status::NotStarted | Status::Started))
    }

    pub fn start_exercise(&mut self, exercise: &Exercise) {
        assert!(
            self.0.remove(exercise), // remove returns true if the item existed
            "state did not contain the exercise"
        );

        let mut exercise = exercise.clone();
        exercise.exercise_state.start_exercise();
        self.0.insert(exercise);
    }

    pub fn complete_exercise(&mut self, exercise: &Exercise) {
        assert!(
            self.0.remove(exercise), // remove returns true if the item existed
            "state did not contain the exercise"
        );

        let mut exercise = exercise.clone();
        exercise.exercise_state.complete_exercise();
        self.0.insert(exercise);
    }

    /// Finds the best matching exercise from the state
    pub fn match_exercise(&self, path: impl AsRef<Path>) -> Option<&Exercise> {
        let path = path
            .as_ref()
            .strip_prefix("exercises")
            .unwrap_or_else(|_| path.as_ref());

        if let Some(e) = self.0.iter().find(|exercise| exercise.path == path) {
            Some(e)
        } else if let Some(e) = self.0.iter().find(|exercise| exercise.path.ends_with(path)) {
            Some(e)
        } else if let Some(e) = self
            .0
            .iter()
            .filter(|ex| ex.path.to_str().unwrap().contains(path.to_str().unwrap()))
            // Next unfineshed
            .find(|ex| matches!(ex.status(), Status::NotStarted | Status::Started))
        {
            Some(e)
        } else {
            None
        }
    }
}
