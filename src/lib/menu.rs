use crate::state::Exercise;
use colored::ColoredString;
use colored::Colorize;
use core::fmt;
use crossterm::cursor;
use crossterm::execute;
use crossterm::terminal::Clear;
use crossterm::terminal::ClearType;
use itertools::Itertools;
use lazy_static::lazy_static;
use std::collections::BTreeSet;
use std::fmt::Display;
use std::io::Write;

// TODO add windows compatible characters
lazy_static! {
    static ref CHEVRON: ColoredString = "❯".purple();
    static ref STARTED: ColoredString = "×".yellow();
    static ref COMPLETED: ColoredString = "✓".green();
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Status {
    NotStarted,
    Started,
    Completed,
}

impl Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Status::{Completed, NotStarted, Started};
        match *self {
            NotStarted => Ok(()),
            Started => write!(f, "{}", *STARTED),
            Completed => write!(f, "{}", *COMPLETED),
        }
    }
}

pub fn draw_menu(
    output: &mut impl Write,
    items: BTreeSet<Exercise>,
    sel: usize,
) -> anyhow::Result<()> {
    execute!(
        output,
        Clear(ClearType::All),
        cursor::Hide,
        cursor::MoveTo(0, 0)
    )?;

    let selected_exercise = items.iter().nth(sel).expect("sel to be in range");

    let grouped = items.iter().group_by(|t| t.folder_name());

    // loop over each folder
    for (folder, exercises_in_folder) in &grouped {
        let exercises_in_folder: Vec<_> = exercises_in_folder.collect();

        let folder_statuses: Vec<_> = exercises_in_folder.iter().map(|e| e.status()).collect();
        let contains_not_started = folder_statuses.contains(&Status::NotStarted);
        let contains_started = folder_statuses.contains(&Status::Started);
        let contains_completed = folder_statuses.contains(&Status::Completed);

        let folder_status = match (contains_not_started, contains_started, contains_completed) {
            // Folder contains no started or completed exercises
            (true, false, false) => Status::NotStarted,
            // Folder contains only completed exercises
            (false, false, true) => Status::Completed,
            (false, false, false) => panic!("no exercises in folder"),
            (_, _, _) => Status::Started,
        };
        write!(output, "   {}/ {}\n\r", folder, folder_status)?;

        // if the week is the selected week
        if selected_exercise.folder_name() == folder {
            // loop over all exercises in the week
            for exercise in exercises_in_folder {
                if exercise == selected_exercise {
                    write!(
                        output,
                        " {}   {} {}\n\r",
                        *CHEVRON,
                        exercise.file_name().bright_cyan(),
                        exercise.status()
                    )?;
                } else {
                    write!(
                        output,
                        "     {} {}\n\r",
                        exercise.file_name(),
                        exercise.status()
                    )?;
                }
            }
        }
    }

    Ok(())
}
