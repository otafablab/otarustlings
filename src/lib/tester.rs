use std::{any::Any, io::stdout, marker::PhantomData, path::Path, sync::mpsc::Receiver};

use crossterm::{
    cursor,
    event::{Event, KeyCode, KeyEvent},
    execute,
    style::{Color, Print, ResetColor, SetForegroundColor},
    terminal::{enable_raw_mode, Clear, ClearType},
    ExecutableCommand,
};

use std::fmt::Debug;
use tracing::debug;

use crate::{
    exercise::{cargo_test, compile, run, CompileError, RunError},
    menu::draw_menu,
    state::{Exercise, State},
    utils::{filter_cargo, filter_rs},
    Message,
};

#[derive(Debug)]
pub struct TesterStateData {
    selected_exercise: Exercise,
    state: State, // TODO rename
}

#[derive(Debug)]
pub struct InMenu;
#[derive(Debug)]
pub struct WatchingExercise;
#[derive(Debug)]
pub struct Terminating;

#[repr(C)]
pub struct TesterState<T: Debug> {
    state: TesterStateData,
    marker: PhantomData<T>,
}

impl<T: Debug> Debug for TesterState<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct(&format!("TesterState<{}>", std::any::type_name::<T>()))
            .field("state", &self.state)
            .field("marker", &self.marker)
            .finish()
    }
}

pub trait StateMachine: Debug {
    fn handle_message(self: Box<Self>, msg: Message) -> anyhow::Result<Box<dyn StateMachine>>;

    fn as_any(&self) -> &dyn Any;
}

impl TesterState<InMenu> {
    fn handle_keyevent(
        mut self: Box<Self>,
        key: Event,
    ) -> (anyhow::Result<Box<dyn StateMachine>>, bool) {
        match key {
            Event::Key(KeyEvent {
                code: KeyCode::Down,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Char('j'),
                ..
            }) => {
                self.state.selected_exercise = self
                    .state
                    .state
                    .next_exercise(&self.state.selected_exercise.clone())
                    .clone();

                (self.handle_message(Message::Draw), false)
            }
            Event::Key(KeyEvent {
                code: KeyCode::Esc, ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Backspace,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Left,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Char('q'),
                ..
            }) => {
                // Should break the input loop
                (self.handle_message(Message::Terminate), true)
            }
            Event::Key(KeyEvent {
                code: KeyCode::Up, ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Char('k'),
                ..
            }) => {
                self.state.selected_exercise = self
                    .state
                    .state
                    .prev_exercise(&self.state.selected_exercise.clone())
                    .clone();

                (self.handle_message(Message::Draw), false)
            }
            Event::Key(KeyEvent {
                code: KeyCode::PageDown,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Tab, ..
            }) => {
                if let Some(exercise) = self
                    .state
                    .state
                    .next_week_exercise(&self.state.selected_exercise)
                {
                    self.state.selected_exercise = exercise.clone()
                }

                (self.handle_message(Message::Draw), false)
            }
            Event::Key(KeyEvent {
                code: KeyCode::PageUp,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::BackTab,
                ..
            }) => {
                if let Some(exercise) = self
                    .state
                    .state
                    .prev_week_exercise(&self.state.selected_exercise)
                {
                    self.state.selected_exercise = exercise.clone()
                }

                (self.handle_message(Message::Draw), false)
            }
            Event::Key(KeyEvent {
                code: KeyCode::Enter,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Right,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Char(' '),
                ..
            }) => (self.handle_message(Message::TestExercise), false),
            _ => (self.handle_message(Message::Draw), false),
        }
    }
}

impl StateMachine for TesterState<InMenu> {
    fn handle_message(mut self: Box<Self>, msg: Message) -> anyhow::Result<Box<dyn StateMachine>> {
        match msg {
            Message::SelectExercise(e) => {
                self.state.selected_exercise = e;
                Ok(self)
            }
            Message::TestExercise => {
                // SAFETY: TesterState<WatchingExercise> has the same size,
                // layout and alignment as TesterState<InMenu> due to repr(C)
                let watching = unsafe {
                    Box::from_raw(Box::into_raw(self) as *mut TesterState<WatchingExercise>)
                };
                // TODO not sure it makes sense to use TestExercise
                watching.handle_message(Message::TestExercise)
            }
            Message::Notify(_) => Ok(self), // Ignore changes
            Message::KeyEvent(key, terminator) => {
                let (sölf, should_terminate) = self.handle_keyevent(key);
                terminator.send(should_terminate)?;
                sölf
            }
            Message::Draw => {
                draw_menu(
                    &mut stdout(),
                    self.state.state.0.clone(),
                    self.state
                        .state
                        .exercise_index(&self.state.selected_exercise),
                )?;
                Ok(self)
            }
            Message::ExitExercise => {
                panic!("cannot exit exercise in menu")
            }
            Message::Terminate => {
                debug!("terminating tester");

                // SAFETY: TesterState<InMenu> has the same size,
                // layout and alignment as TesterState<Terminating> due to repr(C)
                Ok(unsafe { std::mem::transmute::<_, Box<TesterState<Terminating>>>(self) })
            }
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl TesterState<WatchingExercise> {
    fn handle_keyevent(mut self: Box<Self>, key: Event) -> anyhow::Result<Box<dyn StateMachine>> {
        match key {
            Event::Key(KeyEvent {
                code: KeyCode::Esc, ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Backspace,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Left,
                ..
            })
            | Event::Key(KeyEvent {
                code: KeyCode::Char('q'),
                ..
            }) => self
                .handle_message(Message::ExitExercise)?
                .handle_message(Message::Draw),
            Event::Key(KeyEvent {
                code: KeyCode::Enter,
                ..
            }) => {
                if let Some(next) = self
                    .state
                    .state
                    .get_next_exercise_of_week(&self.state.selected_exercise)
                {
                    self.state.selected_exercise = next.clone();
                    self.handle_message(Message::TestExercise)
                } else {
                    if let Some(next_week) = self
                        .state
                        .state
                        .next_week_exercise(&self.state.selected_exercise)
                    {
                        self.state.selected_exercise = next_week.clone();
                    }

                    self.handle_message(Message::ExitExercise)?
                        .handle_message(Message::Draw)
                }
            }
            _ => Ok(self),
        }
    }
}

impl StateMachine for TesterState<WatchingExercise> {
    fn handle_message(mut self: Box<Self>, msg: Message) -> anyhow::Result<Box<dyn StateMachine>> {
        match msg {
            // SAFETY: no idea if it works
            Message::ExitExercise => {
                // SAFETY: TesterState<InMenu> has the same size,
                // layout and alignment as TesterState<WatchingExercise> due to repr(C)
                let inmenu =
                    unsafe { Box::from_raw(Box::into_raw(self) as *mut TesterState<InMenu>) };
                Ok(inmenu)
            }
            Message::KeyEvent(key, terminator) => {
                terminator.send(false)?;
                self.handle_keyevent(key)
            }
            Message::TestExercise => {
                execute!(
                    stdout(),
                    Clear(ClearType::All),
                    cursor::Hide,
                    cursor::MoveTo(0, 0),
                    SetForegroundColor(Color::Blue),
                    Print("\r[*] ".to_string()),
                    // Reset to default colors
                    ResetColor,
                    Print("Compiling...\r\n".to_string()),
                )?;

                self.state
                    .state
                    .start_exercise(&self.state.selected_exercise);
                self.state.state.save()?;

                let exercise_path = Path::new("exercises").join(&self.state.selected_exercise.path);

                let cargo_toml_path = exercise_path.join("Cargo.toml");
                let ok_output = if cargo_toml_path.exists() {
                    match cargo_test(cargo_toml_path) {
                        Ok(output) => output,
                        Err(RunError::OutputError(output)) => {
                            // TODO join stderr and stdout for cargo_test
                            println!("{}", output.stderr.replace('\n', "\r\n"));
                            println!("{}", output.stdout.replace('\n', "\r\n"));
                            return Ok(self);
                        }
                        Err(e) => {
                            panic!("{e:?}");
                        }
                    }
                } else {
                    match compile(exercise_path) {
                        Ok(tempfile) => match run(tempfile.into_temp_path()) {
                            Ok(output) => output,
                            Err(RunError::OutputError(output)) => {
                                println!("{}", output.stdout.replace('\n', "\r\n"));
                                return Ok(self);
                            }
                            Err(e) => {
                                panic!("{e:?}");
                            }
                        },
                        Err(CompileError::OutputError(output)) => {
                            println!("{}", output.stderr.replace('\n', "\r\n"));

                            return Ok(self);
                        }
                        Err(e) => {
                            println!("{e:?}");
                            return Ok(self);
                        }
                    }
                };

                execute!(
                    stdout(),
                    Print(ok_output.stdout.replace('\n', "\r\n")),
                    SetForegroundColor(Color::Green),
                    Print("\r\n[+] ".to_string()),
                    ResetColor,
                    Print(
                        "Well done! Press Q to return to menu or ENTER to continue\r\n".to_string()
                    ),
                )?;

                self.state
                    .state
                    .complete_exercise(&self.state.selected_exercise);
                self.state.state.save()?;

                Ok(self)
            }
            Message::Notify(changed_path) => {
                debug!("notify: {:?} changed", changed_path);
                if (filter_rs(&changed_path)
                    || filter_cargo(
                        changed_path
                            .parent()
                            .expect("changed_path to have a parent"),
                    ))
                    && changed_path.starts_with(&self.state.selected_exercise.path)
                {
                    self.handle_message(Message::TestExercise)
                } else {
                    Ok(self)
                }
            }
            Message::Terminate => {
                debug!("terminating tester");

                // SAFETY: TesterState<WatchingExercise> has the same size,
                // layout and alignment as TesterState<Terminating> due to repr(C)
                let terminating =
                    unsafe { Box::from_raw(Box::into_raw(self) as *mut TesterState<Terminating>) };
                Ok(terminating)
            }
            todo => todo!("{:?}", todo),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl StateMachine for TesterState<Terminating> {
    fn handle_message(self: Box<Self>, _msg: Message) -> anyhow::Result<Box<dyn StateMachine>> {
        panic!("state machine is terminating")
    }

    fn as_any(&self) -> &dyn Any {
        self
    }
}

impl TesterState<InMenu> {
    pub fn new() -> anyhow::Result<Box<Self>> {
        let state = State::scan_open_default("exercises")?;
        let selected_exercise = state
            .first_unfinished()
            .unwrap_or_else(|| state.0.iter().next().expect("state to contain exercises"))
            .clone();

        Ok(Box::new(Self {
            state: TesterStateData {
                state,
                selected_exercise,
            },
            marker: PhantomData,
        }))
    }

    pub fn start_exercise(self) -> TesterState<WatchingExercise> {
        TesterState::<WatchingExercise> {
            state: self.state,
            marker: PhantomData,
        }
    }
}

impl TesterState<WatchingExercise> {
    pub fn back_to_menu(self) -> TesterState<InMenu> {
        TesterState {
            state: self.state,
            marker: PhantomData,
        }
    }
}

/// Worker loop for the otarustlings state machine. Receives [`Message`]s from
/// [`crate::utils::notify_adapter`] and [`crate::utils::transmit_input`].
///
/// The `sender` is used send self-dispatched messages, so has to be connected
/// to the `receiver`.
///
/// # Panics
///
/// - If the mutex of `state` is poisoned.
///
/// # Errors
///
/// The [`anyhow::Error`] may contain an [`std::io::Error`], a
/// [`std::sync::mpsc::SendError`].
pub fn handle_test(receiver: &Receiver<Message>) -> anyhow::Result<()> {
    let mut ts: Box<dyn StateMachine> = TesterState::new()?;

    enable_raw_mode()?;

    ts = ts.handle_message(Message::Draw)?;
    loop {
        ts = ts.handle_message(receiver.recv()?)?;
        if ts
            .as_any()
            .downcast_ref::<TesterState<Terminating>>()
            .is_some()
        {
            stdout().execute(cursor::Show)?;
            break;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn terminates_in_menu() {
        let ts = TesterState::new().unwrap();
        let ts = ts.handle_message(Message::Terminate).unwrap();
        assert!(ts
            .as_any()
            .downcast_ref::<TesterState<Terminating>>()
            .is_some());
    }

    #[test]
    fn test_exercise() {
        let ts = TesterState::new().unwrap();
        assert_eq!(
            ts.state.selected_exercise.path.to_str(),
            Some("week1/01-quiz.rs")
        );
        let ts = ts.handle_message(Message::TestExercise).unwrap();
        assert!(ts
            .as_any()
            .downcast_ref::<TesterState<WatchingExercise>>()
            .is_some());
    }

    #[test]
    fn terminates_watching() {
        let ts = TesterState::new().unwrap();
        let ts = ts.handle_message(Message::TestExercise).unwrap();
        let ts = ts.handle_message(Message::Terminate).unwrap();
        assert!(ts
            .as_any()
            .downcast_ref::<TesterState<Terminating>>()
            .is_some());
    }

    #[test]
    fn test_exercise_exit() {
        let ts = TesterState::new().unwrap();
        let ts = ts.handle_message(Message::TestExercise).unwrap();
        let ts = ts.handle_message(Message::ExitExercise).unwrap();
        let ts = ts
            .as_any()
            .downcast_ref::<TesterState<InMenu>>()
            .expect("ts to be InMenu");
        assert_eq!(
            ts.state.selected_exercise.path.to_str(),
            Some("week1/01-quiz.rs")
        );
    }
}
