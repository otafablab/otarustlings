use crate::RUSTC_COLOR_ARGS;
use rust_embed::RustEmbed;
use std::{
    fmt::{Display, Formatter},
    fs::{create_dir_all, File},
    io::{self, Write},
    path::{Path, PathBuf},
    process,
    process::Command,
};
use tempfile::NamedTempFile;
use thiserror::Error;
use tracing::{debug, trace};

/// `Output` contains stdout and stderr strings of some executed process.
#[derive(Debug, Clone)]
pub struct Output {
    pub stdout: String,
    pub stderr: String,
}

impl Output {
    pub fn from_cmd_output(output: &process::Output) -> Self {
        Self {
            stdout: String::from_utf8_lossy(&output.stdout).to_string(),
            stderr: String::from_utf8_lossy(&output.stderr).to_string(),
        }
    }
}

impl Display for Output {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), std::fmt::Error> {
        write!(formatter, "{}", self.stdout)
    }
}

/// Represents different errors from [`compile`]
#[derive(Error, Debug)]
pub enum CompileError {
    /// Compilation failed due to an IO error
    #[error("compilation IO error")]
    IOError(#[from] io::Error),

    /// Compilation error which contains compiler output
    #[error("compilation failed")]
    OutputError(Output),
}

/// Represents different errors from [`run`]
#[derive(Error, Debug)]
pub enum RunError {
    /// Execution failed due to an IO error
    #[error("execution IO error")]
    IOError(#[from] io::Error),

    /// Execution failed at runtime. For example the tests may have
    /// failed or the application may have crashed
    #[error("execution failed")]
    OutputError(Output),
}

/// Compiles the Rust source code file with `rustc` and returns a
/// [`NamedTempFile`] guarding the resultant binary.
///
/// # Panics
///
/// Panics if path contains invalid UTF-8.
///
/// # Errors
///
/// If `rustc` exited with non-zero status, it is assumed to be a
/// compilation IO error [`CompileError::IOError`] which is then returned.
pub fn compile(path: impl AsRef<Path>) -> Result<NamedTempFile, CompileError> {
    let temp_file = NamedTempFile::new()?;
    debug!("compiling {:?}", &temp_file);
    let cmd_output = Command::new("rustc")
        .args(&[
            "--test",
            "--edition",
            "2021",
            path.as_ref().to_str().unwrap(),
            "-o",
            temp_file.path().as_os_str().to_str().unwrap(),
        ])
        .args(RUSTC_COLOR_ARGS)
        .output()?;

    if cmd_output.status.success() {
        Ok(temp_file)
    } else {
        Err(CompileError::OutputError(Output::from_cmd_output(
            &cmd_output,
        )))
    }
}

/// Tests the cargo project at `manifest_path`.
///
/// # Panics
///
/// Panics if `manifest_path` contains invalid UTF-8.
///
/// # Errors
///
/// If `cargo test` failed due to an IO error a [`RunError::IOError`] is
/// returned. If the testing itself failed, a [`RunError::OutputError`] is
/// returned.
pub fn cargo_test(manifest_path: impl AsRef<Path>) -> Result<Output, RunError> {
    let mut cmd = Command::new("cargo");
    cmd.args(RUSTC_COLOR_ARGS)
        .args(&[
            "test",
            "--manifest-path",
            manifest_path.as_ref().to_str().unwrap(),
            "--",
            "--show-output",
        ])
        .args(RUSTC_COLOR_ARGS);

    trace!("cargo test: {:?}", cmd);
    // TODO stream output
    let cmd_output = cmd.output()?;

    let output = Output::from_cmd_output(&cmd_output);
    trace!("cargo test output: {:?}", output);
    trace!("cargo test status: {}", cmd_output.status);
    if cmd_output.status.success() {
        Ok(output)
    } else {
        Err(RunError::OutputError(output))
    }
}

/// Executes the file.
///
/// # Errors
///
/// If the execution failed due to an IO error a [`RunError::IOError`]
/// is returned. If the execution failed at runtime a
/// [`RunError::OutputError`].
pub fn run<T: AsRef<Path>>(path: T) -> Result<Output, RunError> {
    debug!("running {:?}", path.as_ref());
    let cmd_output = Command::new(path.as_ref())
        .arg("--show-output")
        .args(RUSTC_COLOR_ARGS)
        .output()?;

    let output = Output::from_cmd_output(&cmd_output);
    if cmd_output.status.success() {
        Ok(output)
    } else {
        Err(RunError::OutputError(output))
    }
}

fn cargo_hack_toml(path: PathBuf) -> PathBuf {
    if path.file_name().expect("file name to be valid") == "Cargo-hack.toml" {
        path.parent()
            .expect("path to have a parent")
            .join(Path::new("Cargo.toml"))
    } else {
        path
    }
}

/// Structure for scanning all of the exercises from the `exercises/`
/// folder at compile time
#[derive(RustEmbed)]
#[folder = "exercises"]
#[exclude = "state.toml"]
#[exclude = "**/target/**/*"]
pub struct Exercises;

/// Initializes the `exercises` folder with the exercises embedded in the
/// `otarustlings` binary. If new exercises are found, a yes/no prompt is shown.
///
/// # Panics
///
/// - If the exercises contain invalid UTF-8.
///
/// # Errors
///
/// An [`io::Error`] is returned on IO errors for example when writing the
/// exercises.
pub fn init() -> io::Result<()> {
    // Get iterator over the exercise files embedded in the binary
    let exercise_files = Exercises::iter();
    let mut new_exercises = Vec::new();

    // Add exercise to new_exercises if it doesn't exist in the file system
    for exercise_file in exercise_files {
        trace!("embedded file: {exercise_file:?}");
        if !Path::new("exercises").join(exercise_file.as_ref()).exists() {
            new_exercises.push(exercise_file);
        }
    }

    if !new_exercises.is_empty() {
        // Ask user before writing new exercise files
        if true
        // FIXME confirmation -> tester
        // Confirm::new()
        // .with_prompt("New exercises available. Do you want to add them?")
        // .default(true)
        // .interact()?
        {
            for exercise_file in new_exercises {
                let path = cargo_hack_toml(Path::new("exercises").join(exercise_file.as_ref()));
                create_dir_all(path.parent().unwrap())?;
                let mut file = File::create(&path)?;
                let exercise = Exercises::get(exercise_file.as_ref())
                    .expect("exercise_file to be in Exercises");
                let data = std::str::from_utf8(&exercise.data).unwrap();
                debug!("writing to {:?}", path);
                file.write_all(data.as_bytes())?;
                println!(
                    "New exercise: {}",
                    cargo_hack_toml(path)
                        .to_str()
                        .expect("path to be valid str")
                );
            }
        } else {
            println!("No new exercises added");
        }
    }
    println!("Run `otarustlings start` to start.");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn compile_success() {
        let compile = compile("tests/fixture/compileSuccess.rs").unwrap();
        let out = run(compile.into_temp_path()).unwrap();
        assert!(out.stdout.contains("Hello, world!"));
        assert_eq!(out.stderr.len(), 0);
    }

    #[test]
    fn compile_failure() {
        let out = compile("tests/fixture/compileFailure.rs").unwrap_err();
        if let CompileError::OutputError(output) = out {
            assert!(output.stderr.contains("aborting due to 2 previous errors"));
            assert!(output.stderr.contains("not a function"));
            assert_eq!(output.stdout.len(), 0);
        } else {
            unreachable!();
        }
    }
}
