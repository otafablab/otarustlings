use anyhow::{bail, Context};
use clap::{Parser, Subcommand};
use notify::{Error, RecommendedWatcher, RecursiveMode, Watcher};
use otarustlings::{
    exercise::init,
    state::State,
    tester::handle_test,
    utils::{notify_adapter, transmit_input},
    Message,
};
use std::{
    io::ErrorKind,
    path::PathBuf,
    sync::mpsc::channel,
    thread::{self},
    time::Duration,
};
use tracing::debug;

/// Otarustlings exercise platform.
///
/// Otarustlings is used for learning hands on Rust with great examples and
/// interesting challenges. To get started, run `otarustlings start`. When new
/// exercises are available, run `otarustlings init` to get them. More
/// information over at https://docs.rs/otarustlings/latest/otarustlings/
#[derive(Parser, Debug)]
#[clap(author = "Otaniemen lukio")]
#[clap(version)] // Shows version automatically
#[clap(propagate_version = true)]
enum Opt {
    /// Writes the exercises to `exercises/` folder. Doesn't overwrite
    /// old exercises.
    Init,
    /// Starts testing otarustlings exercises
    ///
    /// This is the command you would use to interactively start doing
    /// otarustlings
    Start {
        /// Optional path to the exercise. If the path is just the week name,
        /// the first unfinished exercise from that week is selected. The prefix
        /// `exercises/` can be omitted from the path.
        #[structopt(parse(from_os_str))]
        path: Option<PathBuf>,
    },
    /// Noninteractively tests exercises and exits
    Test {
        /// Optional path to the exercise. If the path is just the week name,
        /// the first unfinished exercise from that week is selected. The prefix
        /// `exercises/` can be omitted from the path.
        #[structopt(parse(from_os_str))]
        path: Option<PathBuf>,
    },
    #[clap(subcommand)]
    State(StateOpt),
}

/// Change the internal state of otarustlings
///
/// Usually not needed during normal operation
#[derive(Subcommand, Debug)]
enum StateOpt {
    /// Reset the state file. Does not delete or change any exercises
    Reset,
}

fn main() -> anyhow::Result<()> {
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    match Opt::parse() {
        Opt::State(StateOpt::Reset) => {
            let state = State::scan("exercises");
            state.save()?;
            println!("state file has been reset to default");
        }
        Opt::Init => init().unwrap(),
        Opt::Test { path } => {
            let (sender, receiver) = channel();

            if let Some(path) = path {
                let exercise = State::scan_open_default("exercises")?
                    .match_exercise(&path)
                    .ok_or_else(|| {
                        anyhow::anyhow!("could not find an unfinished exercise {:?}", path)
                    })?
                    .clone();

                // map_err is required because Message is not Sync
                sender.send(Message::SelectExercise(exercise)).unwrap();
                sender.send(Message::TestExercise).unwrap();
                sender.send(Message::Terminate).unwrap();
            } else {
                todo!("test all exercises");
            }

            handle_test(&receiver)?;
        }
        Opt::Start { path } => {
            // TODO check if initialized

            let (tx, rx) = channel();

            let mut watcher = RecommendedWatcher::new(tx, Duration::from_secs(1))?;

            match watcher.watch("exercises", RecursiveMode::Recursive) {
                Err(Error::Io(ioerror)) if ioerror.kind() == ErrorKind::NotFound => {
                    bail!("`exercises` folder not found. Run `otarustlings init` first.")
                }
                _ => {}
            };

            let (sender, receiver) = channel();

            // If start command contains a path
            if let Some(path) = path {
                let exercise = State::scan_open_default("exercises")?
                    .match_exercise(&path)
                    .ok_or_else(|| {
                        anyhow::anyhow!("could not find an unfinished exercise {:?}", path)
                    })?
                    .clone();

                // map_err is required because Message is not Sync
                sender.send(Message::SelectExercise(exercise)).unwrap();
                sender.send(Message::TestExercise).unwrap();
            }

            let watch_handle = {
                let sender = sender.clone();
                thread::spawn(move || notify_adapter(&rx, &sender))
            };
            let input_handle = thread::spawn(move || transmit_input(&sender));

            handle_test(&receiver)?;

            debug!("waiting to join input_handle");
            input_handle
                .join()
                .unwrap()
                .context("joining input_handle failed")?;
            drop(watcher); // Dropping the watcher drops `tx` which causes an error in `rx`
            debug!("waiting to join watch_handle");
            watch_handle
                .join()
                .unwrap()
                .context("joining watch_handle failed")?;
        }
    };
    Ok(())
}
